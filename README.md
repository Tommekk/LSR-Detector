# LSR-Detektor

## Beschreibung

Dieses Projekt simuliert einen geplanten optischen Photonendetektor für den LSR/CRYRING am GSI in Darmstadt.

## Struktur

- Unter `geometry/` sind Geometrieeinstellungen und deren Ergebnisse bei der Datenaufnahme
- Unter `plots/` sind manche der Daten aus geometry/ geplottet
- Unter `includes/` sind die Headerdateien
- Unter `src/` ist der Quellcode

## Projekt kompilieren

Das Projekt wird mit CMake erstellt.

Zum Beispiel
```bash
    $ mkdir ~/build-LSR
    $ cd ~/build-LSR
    $ cmake Path-to-LSR-Detector-dir
    $ make
    $ ./lsr-detector
```

Wichtig ist, dass vorher Geant4 und Root gesourct wurde.
Deshalb muss folgendes vorher im Terminal ausgeführt werden:
```bash
    $ . Path-to-Geant4-install/bin/geant4.sh
    $ . Path-to-ROOT/bin/thisroot.sh
```
