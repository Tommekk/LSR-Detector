/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modification date: 18.2.09
 *
 ***********************************************************/

#include "lsr/DetectorConstruction.hh"
#include "Constants.hh"
#include "lsr/CompoundParabolicConcentrator.hh"

#include "G4SystemOfUnits.hh"

#include "G4Ellipsoid.hh"
#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Torus.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Paraboloid.hh"
#include "G4EllipticalCone.hh"
#include "G4UnionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4EllipticalTube.hh"
#include "G4Ellipsoid.hh"
#include "G4Polyhedra.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4NistManager.hh"
#include "G4LogicalSkinSurface.hh"

#include "globals.hh"

// include actual routines defining the mirror setups
//#include "lsr_SegmentedMirrorConstruction.cpp"
//#include "lsr_HammenDetectorConstruction.cpp"

namespace lsr {

/*********************************************************
 * Constructor and destructor
 *********************************************************/
DetectorConstruction::DetectorConstruction(){;}

DetectorConstruction::~DetectorConstruction(){;}


/*********************************************************
 * Construct experimental setup
 *********************************************************/
::G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // define materials and surfaces
    DefineMaterials();

    /////////////////////////////////////////////////////////////////////
    //----------------------------------
    // detector setups
    //----------------------------------
    // New, Michael Hammen-like mirror

    double beamhole_radius      = gpar->beamhole_radius*mm;

    double focus_ellipse_height = gpar->focus_ellipse_height*mm;
    double focus_ellipse_width  = std::sqrt(2 * beamhole_radius * focus_ellipse_height
                                            - beamhole_radius*beamhole_radius)+10*mm;//detector_width-5*mm;

    //detector_length-5*mm;//2*detector_length;
    //lineare Exzentriziät. Entspricht Abstand der Brennpunkte von der Mitte der Ellipse
    double focus_ellipse_eccentricity = std::sqrt(focus_ellipse_height*focus_ellipse_height
                                                  - focus_ellipse_width*focus_ellipse_width);

    double detector_width       = focus_ellipse_width+10*mm;
    double detector_height      = 2*focus_ellipse_height - beamhole_radius + 10*mm;//gpar->detector_height*mm;
    double detector_length      = gpar->detector_length*mm;
    double detector_position_z  = 0; //Position of the complete detector setup


    double beamhole_length      = detector_length+5*mm;

    double focus_ellipse_length = detector_length-5*mm;

    //----------------------
    // Compund Parabolic Concentrator (CPC)
    //---------------------
    double CPC_acceptanceAngle = gpar->CPC_acceptanceAngle*deg; // acceptanceAngle
    int    CPC_areas           = gpar->CPC_areas;     // How many areas approx the CPC?
    //----------------------
    // viewport window (at the top)
    //----------------------
    double vwp_wrad   = gpar->vwp_wrad;  // radius of viewport windows
    double vwp_wthk   = gpar->vwp_wthk;  // thickness of viewport windows
    double vwp_height = gpar->vwp_height;// height of viewport
    double vwp_frad   = gpar->vwp_frad;  // radius of viewport flange
    double vwp_fthk   = gpar->vwp_fthk;  // thickness of viewport flange

    //----------------------------------
    // experimental hall (world volume)
    //----------------------------------

    // beam line along z axis
    G4Box* experimentalHall_box =
            //new G4Box("expHall_box", 1.0*m, 1.0*m, gpar->s_beam*m/2);//gpar->s_beam*m);
            new G4Box("expHall_box", detector_width, 2.0*m, gpar->s_beam*m/2);
    experimentalHall_log =
            new G4LogicalVolume(experimentalHall_box, Vacuum, "expHall_log");

    // Instantiation of a set of visualization attributes
    G4VisAttributes * Hall_VisAtt = new G4VisAttributes();
    // Set the forced solid style
    Hall_VisAtt->SetForceWireframe(true);
    // Assignment of the visualization attributes to the logical volume
    experimentalHall_log->SetVisAttributes(Hall_VisAtt);

    experimentalHall_phys = new G4PVPlacement(0, G4ThreeVector(),
                                              experimentalHall_log,
                                              "expHall",0,false,0);



    //----------------------
    // Compund Parabolic Concentrator (CPC)
    //---------------------
    CompoundParabolicConcentrator* CPC = new CompoundParabolicConcentrator(vwp_wrad, CPC_acceptanceAngle);
    double CPC_position_y = focus_ellipse_height
            * std::sqrt(1-CPC->getEntryRadius()*CPC->getEntryRadius()/(focus_ellipse_width*focus_ellipse_width))
            + focus_ellipse_eccentricity
            + CPC->getHeight();//detector_height+CPC->getHeight()
    G4RotationMatrix* RotX90 = new G4RotationMatrix();
    RotX90->rotateX(G4double(-90.*deg));
    G4Box* detector_box = new G4Box("detector_box", detector_width, detector_height, detector_length);
    G4Tubs* beamhole = new G4Tubs("beamhole",
                                  0.0,        // inner radius
                                  beamhole_radius,   // outer radius
                                  beamhole_length,   // half height along z-axis
                                  0.*deg,     // start angle
                                  360.*deg    // spanning angle
                                  );
    //Substract tube for the beam
    G4SubtractionSolid* detector_box_with_beamhole =
            new G4SubtractionSolid("detector_box_with_beamhole", detector_box,  beamhole);//, new G4RotationMatrix(),
    //G4ThreeVector(0, 0, G.lid_dist));
    //Cut off ellipse
    G4EllipticalTube* transfer_ellipse = new G4EllipticalTube("transfer_ellipse",focus_ellipse_width, focus_ellipse_height, focus_ellipse_length);
    //G4Ellipsoid* transfer_ellipse = new G4Ellipsoid("transfer_ellipse",focus_ellipse_width, focus_ellipse_height, focus_ellipse_length, 0, 0);
    G4SubtractionSolid* detector_pre =  new G4SubtractionSolid("detector_pre", detector_box_with_beamhole,
                                                           transfer_ellipse, G4TranslateY3D(focus_ellipse_eccentricity));
    // Make hole for CPC
    G4Tubs* detector_hole_CPC =  new G4Tubs("beam_cyl",
                                                              0,     // inner radius
                                                              CPC->getEntryRadius(), // outer radius
                                                              detector_height/2,     // half height along z-axis
                                                              0.*deg,                // start angle
                                                              360.*deg               // spanning angle
                                                              );
    G4SubtractionSolid* detector = new G4SubtractionSolid("detector", detector_pre, detector_hole_CPC, RotX90, G4ThreeVector(0, detector_height, 0));
    G4LogicalVolume* detector_log = new G4LogicalVolume(detector, Steel, "detector_log");
    new G4LogicalSkinSurface("detector_mirror_surface", detector_log, MIRO2_surf);
    G4VisAttributes * detector_VisAtt = new G4VisAttributes();
    detector_VisAtt->SetForceWireframe(true);
    detector_VisAtt->SetForceAuxEdgeVisible(true);
    detector_log->SetVisAttributes(detector_VisAtt);
    new G4PVPlacement(NULL, G4ThreeVector(0., 0., detector_position_z), detector_log,"detector", experimentalHall_log, false, 0);
    ///////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////
    // Detector Definition

    //----------------------
    // viewport window (at the top)
    //----------------------
    G4Tubs* win2 = new G4Tubs("win2",
                              0.0,        // inner radius
                              vwp_wrad,   // outer radius
                              vwp_wthk/2, // half height along z-axis
                              0.*deg,     // start angle
                              360.*deg    // spanning angle
                              );
    G4LogicalVolume* win2_log = new G4LogicalVolume(win2, Glass, "win2_log");
    // Define visualization attributes with cyan colour
    G4VisAttributes * win2_VisAtt = new G4VisAttributes(G4Colour(1.,1.,0.));
    win2_log->SetVisAttributes(win2_VisAtt);
    // reentrance tubes
    G4Tubs* retub1 = new G4Tubs("retub1",
                                vwp_wrad,     // inner radius
                                vwp_wrad+1.5, // outer radius
                                vwp_height/2, // half height along z-axis
                                0.*deg,       // start angle
                                360.*deg      // spanning angle
                                );
    G4Tubs* retub2 = new G4Tubs("retub2",
                                vwp_wrad+2.0, // inner radius
                                vwp_frad,     // outer radius
                                vwp_fthk/2,   // thickness along z-axis
                                0.*deg,       // start angle
                                360.*deg      // spanning angle
                                );
    G4UnionSolid* retub = new G4UnionSolid("retub", retub1, retub2, NULL,
                             G4ThreeVector(0, 0, -vwp_height/2+vwp_fthk/2));
    G4LogicalVolume* retub_log = new G4LogicalVolume(retub, Steel, "retub_log");
    G4VisAttributes * retub_VisAtt = new G4VisAttributes();
    retub_VisAtt->SetForceWireframe(true);
    retub_VisAtt->SetForceAuxEdgeVisible(true);
    retub_log->SetVisAttributes(retub_VisAtt);

    //-------------
    // lightguides
    //-------------
    double hight_lg = 65*mm;//G.lg_height/2;
    double rad_det  = 23*mm;
    G4Cons* lightguide = new G4Cons("lightguide",
                                    0.0,      // inner radius at -pDz
                                    23,     // outer radius at -pDz
                                    0.0,      // inner radius at +pDz
                                    36,     // outer radius at +pDz
                                    hight_lg,     // half height along z-axis
                                    0.*deg,        // start angle
                                    360.*deg       // spanning angle
                                    );
    G4LogicalVolume* lightguide_log = new G4LogicalVolume(lightguide, PGlass, "lightguide_log");
    // Define visualization attributes with cyan colour
    G4VisAttributes * lightguide_VisAtt = new G4VisAttributes(G4Colour(0.,1.,1.));
    lightguide_log->SetVisAttributes(lightguide_VisAtt);

    //--------------------------
    // pmt's for mirror section (Photomultiplier)
    //--------------------------
    G4Tubs* spmt = new G4Tubs("spmt",
                              0.0*cm,         // inner radius
                              25,     // outer radius
                              80.0/2,   // height along z-axis
                              0.*deg,         // start angle
                              360.*deg        // spanning angle
                              );

    G4LogicalVolume* spmt_log = new G4LogicalVolume(spmt, Glass, "spmt_log");

    // Define visualization attributes with orange colour
    G4VisAttributes * spmt_VisAtt = new G4VisAttributes(G4Colour(1.,0.65,0.));
    //spmt_VisAtt->SetForceWireframe(true);
    // Assignment of the visualization attributes to the logical volume
    spmt_log->SetVisAttributes(spmt_VisAtt);

    //------------------------------------
    // photocathode's for mirror section (between spmt and lightguide)
    //------------------------------------
    // R1017
    G4Tubs* scath = new G4Tubs("scath", 0.0, rad_det, 1.0/2, 0., 360.*deg);
    G4LogicalVolume* scath_log  = new G4LogicalVolume(scath, Steel, "scath_log");
    // add photosensitive surface to volume
    new G4LogicalSkinSurface("scath_surf", scath_log, Photocath_surf);
    // ************************** filter
    G4Tubs* filter = new G4Tubs("filter",
                                0.0*cm,        // inner radius
                                25,     // outer radius
                                1*mm, // thickness along z-axis
                                0.*deg,        // start angle
                                360.*deg       // spanning angle
                                );
    G4LogicalVolume* filter_log = new G4LogicalVolume(filter, Filter, "filter_log");
    // Define visualization attributes with cyan colour
    G4VisAttributes * filter_VisAtt = new G4VisAttributes(G4Colour(1.,0.,0.));
    filter_log->SetVisAttributes(filter_VisAtt);
    //pmt window
    G4Tubs* winpmt = new G4Tubs("winpmt",
                                0.0,        // inner radius
                                25*mm,   // outer radius
                                0.5*mm, // half height along z-axis
                                0.*deg,     // start angle
                                360.*deg    // spanning angle
                                );
    G4LogicalVolume* winpmt_log = new G4LogicalVolume(winpmt, Glass, "winpmt_log");
    // Define visualization attributes with cyan colour
    G4VisAttributes * winpmt_VisAtt = new G4VisAttributes(G4Colour(1.,1.,0.));
    winpmt_log->SetVisAttributes(winpmt_VisAtt);

    //////////////////////////////////////////////////////////////////////////////////
    // CPC Definition (Compound Parabolic Concentrator)
    // Is intended to focus light on the photocathode

    G4Polycone* CPC_polycone = CPC->getG4Polycone("CPC_polycone",CPC_areas);
    G4LogicalVolume* CPC_log = new G4LogicalVolume(CPC_polycone, Steel, "CPC_log");
    G4VisAttributes * CPC_VisAtt = new G4VisAttributes();
    CPC_VisAtt->SetForceWireframe(true);
    CPC_VisAtt->SetForceAuxEdgeVisible(true);
    CPC_log->SetVisAttributes(CPC_VisAtt);
    new G4LogicalSkinSurface("CPC_mirror_surface", CPC_log, MIRO2_surf);
    new G4PVPlacement(RotX90, G4ThreeVector(0, CPC_position_y, detector_position_z), CPC_log,"CPC", experimentalHall_log, false, 0);
    /////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////
    //Placing the elements
    double zpos = detector_position_z;
    double cathode_height = CPC_position_y;
    new G4PVPlacement(RotX90,G4ThreeVector(0, (hight_lg+cathode_height+2)*mm, zpos),
                      lightguide_log, "lightguide", experimentalHall_log, false, 0);
    new G4PVPlacement(RotX90, G4ThreeVector(0, cathode_height+1*mm, zpos),
                      win2_log, "window", experimentalHall_log, false, 0);
    new G4PVPlacement(RotX90, G4ThreeVector(0, cathode_height+(36-8)*mm, zpos),
                      retub_log, "retub",
                      experimentalHall_log, false, 0);
    new G4PVPlacement(RotX90,G4ThreeVector(0, cathode_height+(2+40+1+2*hight_lg+2+1)*mm, zpos),
                      spmt_log, "spmt",
                      experimentalHall_log, false, 0);
    new G4PVPlacement(RotX90,G4ThreeVector(0, cathode_height+(2+0.5+2*hight_lg+2+1)*mm, zpos),   //normale pos: 0, 80+0.5+100, zpos ** pos testmessung: 0, 80+0.5+136, zpos
                      scath_log, "scath",
                      experimentalHall_log, false, 0);
    //////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////
    //----------------------------------
    // beam tube
    //----------------------------------

    // construct cylindrical tube body
    G4Tubs* beam_cyl = new G4Tubs("beam_cyl",
                                  gpar->beam_rad,     // inner radius
                                  gpar->beam_rad+5.0, // outer radius
                                  gpar->s_beam*m/2,   // half height along z-axis
                                  0.*deg,             // start angle
                                  360.*deg            // spanning angle
                                  );
    // make CF100 exits
     G4Tubs* beam_exit = new G4Tubs("beam_exit",
                                 0,               // inner radius
                                 50.0,            // outer radius
                                 10.0,            // half height along z-axis
                                 0.*deg,          // start angle
                                 360.*deg         // spanning angle
                                 );
    G4RotationMatrix* RotBE = new G4RotationMatrix();
    RotBE->rotateX(G4double(90.*deg));
    G4SubtractionSolid* beamtube_pre =
            new G4SubtractionSolid("beamtube_pre", beam_cyl, beam_exit, RotBE, G4ThreeVector(0, gpar->beam_rad, 0));
    //Create hole for detector_box
    G4SubtractionSolid* beamtube = new G4SubtractionSolid("beamtube", beamtube_pre, detector_box);
    G4LogicalVolume* beamtube_log = new G4LogicalVolume(beamtube, Steel, "beamtube_log");
    //add surface to beam pipe
    new G4LogicalSkinSurface("pipe_surface", beamtube_log, Pipe_surf);
    G4VisAttributes * Beamtube_VisAtt = new G4VisAttributes(G4Colour(1.,0.,0.));
    Beamtube_VisAtt->SetForceWireframe(true);
    //Beamtube_VisAtt->SetForceAuxEdgeVisible (true);
    beamtube_log->SetVisAttributes(Beamtube_VisAtt);
    beamtube_phys = new G4PVPlacement(NULL, G4ThreeVector(0, 0, 0),//gpar->s_beam*m/2),
                                  beamtube_log, "beamtube",
                                  experimentalHall_log, false, 0);
    return experimentalHall_phys;
}


/*********************************************************
 * Construct segmented mirror section
 *********************************************************/
void DetectorConstruction::BuildMirrorSection()
{
    /*
    G4LogicalVolume* detector_log = new G4LogicalVolume(detector, Steel, "detector_log");

    new G4LogicalSkinSurface("detector_mirror_surface", detector_log, MIRO2_surf);

    new G4PVPlacement(NULL, G4ThreeVector(0., 0., G.seg_zorig-detector_length/2), detector_log,
                      "detector", experimentalHall_log, false, 0);*/
    // return detector;
}


/*********************************************************
 * Define materials and surfaces
 *********************************************************/
void DetectorConstruction::DefineMaterials()
{
    G4NistManager* man = G4NistManager::Instance();

    const G4int OPT_NUMREAL = 16;

    // Photon energies at:           4130nm, 1550nm,  1000nm,   950nm,   900nm,   850nm,   800nm,   750nm,   700nm,   650nm,  600 nm, 550 nm,   500 nm,   450nm,  400 nm, 350 nm
    G4double Ephoton[OPT_NUMREAL] = {0.3*eV, 0.8*eV, 1.24*eV, 1.31*eV, 1.38*eV, 1.46*eV, 1.55*eV, 1.65*eV, 1.77*eV, 1.91*eV, 2.07*eV, 2.25*eV, 2.48*eV, 2.76*eV, 3.10*eV, 3.55*eV};


    //
    // Air
    //
    Air   = man->FindOrBuildMaterial("G4_AIR");

    G4double Air_RIND[OPT_NUMREAL] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

    G4MaterialPropertiesTable *Air_mt = new G4MaterialPropertiesTable();
    Air_mt->AddProperty("RINDEX", Ephoton, Air_RIND,   OPT_NUMREAL);
    Air->SetMaterialPropertiesTable(Air_mt);

    //
    // Glass G4_Pyrex_Glass G4_GLASS_PLATE
    //
    Glass = man->FindOrBuildMaterial("G4_GLASS_PLATE");

    G4double Glass_RIND[OPT_NUMREAL]  = {1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46};
    G4double Glass_ABSL[OPT_NUMREAL]  = {4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m};

    G4MaterialPropertiesTable *Glass_mt = new G4MaterialPropertiesTable();
    Glass_mt->AddProperty("ABSLENGTH", Ephoton, Glass_ABSL, OPT_NUMREAL);
    Glass_mt->AddProperty("RINDEX",    Ephoton, Glass_RIND, OPT_NUMREAL);
    Glass->SetMaterialPropertiesTable(Glass_mt);


    //
    // Filter
    //
    Filter = man->FindOrBuildMaterial("G4_GLASS_PLATE");

    G4double Filter_RIND[OPT_NUMREAL]  = {1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46};
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  800,  750,  700,  650,  600,  550,  500,  450,  400,  350} [nm]
    G4double Filter_ABSL[OPT_NUMREAL]  =
            // {0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm}; //bandpassfilter rot
            // {4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m}; //kein filter
    {4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm}; //rotfilter wie bei gsi testmessung

    G4MaterialPropertiesTable *Filter_mt = new G4MaterialPropertiesTable();
    Filter_mt->AddProperty("ABSLENGTH", Ephoton, Filter_ABSL, OPT_NUMREAL);
    Filter_mt->AddProperty("RINDEX",    Ephoton, Filter_RIND, OPT_NUMREAL);
    Filter->SetMaterialPropertiesTable(Filter_mt);


    //
    // Plexiglass
    //G4_PLEXIGLASS
    PGlass = man->FindOrBuildMaterial("G4_GLASS_PLATE");
    G4double PGlass_RIND[OPT_NUMREAL]  = {1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49};
    G4double PGlass_ABSL[OPT_NUMREAL]  = {4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m};

    G4MaterialPropertiesTable *PGlass_mt = new G4MaterialPropertiesTable();
    PGlass_mt->AddProperty("ABSLENGTH", Ephoton, PGlass_ABSL, OPT_NUMREAL);
    PGlass_mt->AddProperty("RINDEX",    Ephoton, PGlass_RIND, OPT_NUMREAL);
    PGlass->SetMaterialPropertiesTable(PGlass_mt);

    //
    // Stainless steel
    //
    G4Element* Fe = new G4Element("Iron",   "Fe", 26., 55.8500*g/mole);
    G4Element* C  = new G4Element("Carbon", "C",   6., 12.0110*g/mole);
    G4Element* Co = new G4Element("Cobalt", "Co", 27., 58.9332*g/mole);
    Steel = new G4Material("Steel", 7.7*g/cm3, 3);
    Steel->AddElement(C, 0.04);
    Steel->AddElement(Fe, 0.88);
    Steel->AddElement(Co, 0.08);

    //
    //Vacuum
    //
    G4double density     = 1.e-15*g/cm3;
    G4double pressure    = 1.e-15*bar;
    G4double temperature = 293.*kelvin;
    G4int ncomponents    = 1;
    G4int fractionmass   = 1;
    Vacuum = new G4Material("Vacuum", density, ncomponents, kStateGas, temperature, pressure);
    Vacuum->AddMaterial(Air, fractionmass);
    G4double Vacuum_RIND[OPT_NUMREAL] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

    G4MaterialPropertiesTable *Vacuum_mt = new G4MaterialPropertiesTable();
    Vacuum_mt->AddProperty("RINDEX", Ephoton, Vacuum_RIND, OPT_NUMREAL);
    Vacuum->SetMaterialPropertiesTable(Vacuum_mt);


    //
    // Mirror surface
    //
    Mirror_surf = new G4OpticalSurface("mirror_surface");
    Mirror_surf->SetType(dielectric_metal);
    Mirror_surf->SetFinish(polished);
    Mirror_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  800,  750,  700,  650,  600,  550,  500,  450,  400,  350} [nm]
    G4double Mirror_REFL[OPT_NUMREAL] =
            //{0.95, 0.90, 0.84, 0.82, 0.81, 0.79, 0.81, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93}; //(spiegel altes exp.) MIRO2
            //{0.10, 0.40, 0.65, 0.68, 0.74, 0.78, 0.82, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93}; //(spiegel altes exp.) neu
            //{0.92, 0.92, 0.92, 0.90, 0.84, 0.78, 0.82, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93}; //(spiegel altes exp.) mit minimum
    {0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.93, 0.70, 0.60, 0.55, 0.48, 0.40}; //kupfer/gold
    //{1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00}; //best
    G4MaterialPropertiesTable *Mirror_mt = new G4MaterialPropertiesTable();
    Mirror_mt->AddProperty("REFLECTIVITY", Ephoton, Mirror_REFL, OPT_NUMREAL);

    Mirror_surf->SetMaterialPropertiesTable(Mirror_mt);

    //
    // MIRO2 surface
    //
    MIRO2_surf = new G4OpticalSurface("mirror_surface");
    MIRO2_surf->SetType(dielectric_metal);
    MIRO2_surf->SetFinish(polished);
    MIRO2_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  800,  750,  700,  650,  600,  550,  500,  450,  400,  350} [nm]
    G4double MIRO2_REFL[OPT_NUMREAL] =
    {0.95, 0.90, 0.84, 0.82, 0.81, 0.79, 0.81, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93}; //(spiegel altes exp.) MIRO2
    //{1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00}; //best
    G4MaterialPropertiesTable *MIRO2_mt = new G4MaterialPropertiesTable();
    MIRO2_mt->AddProperty("REFLECTIVITY", Ephoton, MIRO2_REFL, OPT_NUMREAL);

    MIRO2_surf->SetMaterialPropertiesTable(MIRO2_mt);



    // Beam pipe surface
    //
    Pipe_surf = new G4OpticalSurface("pipe_surface");
    Pipe_surf->SetType(dielectric_metal);
    Pipe_surf->SetFinish(polished);
    Pipe_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  800,  750,  700,  650,  600,  550,  500,  450,  400,  350} [nm]
    G4double Pipe_REFL[OPT_NUMREAL] =
    {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00}; //no refectivity
    //{0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25}; //(values from measurement)
    //{0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40}; //(values from paper)
    //{0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55}; //(values buch hans werner)
    //{1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00}; //best
    G4MaterialPropertiesTable *Pipe_mt = new G4MaterialPropertiesTable();
    Pipe_mt->AddProperty("REFLECTIVITY", Ephoton, Pipe_REFL, OPT_NUMREAL);
    Pipe_surf->SetMaterialPropertiesTable(Pipe_mt);

    //
    //Photocathode surface properties
    //
    Photocath_surf= new G4OpticalSurface("photocath_surf");
    Photocath_surf->SetType(dielectric_metal);
    Photocath_surf->SetFinish(polished);
    Photocath_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,    900,    850,    800,    750,    700,    650,    600,    550,    500,    450,    400,    350} [nm]
    G4double Photocath_EFF[OPT_NUMREAL]  =
            //{0.00, 0.00, 0.00, 0.00, 0.0013, 0.0170, 0.0410, 0.0750, 0.1200, 0.1400, 0.1500, 0.1500, 0.1340, 0.0900, 0.0500, 0.0250}; //r1017  selektiert last!
            //{0.00, 0.00, 0.00, 0.00, 0.0000, 0.0100, 0.0400, 0.0900, 0.1300, 0.1500, 0.1600, 0.1600, 0.1400, 0.1200, 0.0700, 0.0400}; //r1017selektiert!
    {0.00, 0.00, 0.00, 0.00, 0.0040, 0.0071, 0.0232, 0.0411, 0.0648, 0.0892, 0.1071, 0.1121, 0.1023, 0.0800, 0.0350, 0.0280}; //r1017
    //{0.00, 0.00, 0.00, 0.00, 0.0056, 0.0099, 0.0325, 0.0575, 0.0972, 0.1249, 0.1499, 0.1569, 0.1432, 0.1120, 0.0490, 0.0390}; //r1017 *1.4
    //{0.00, 0.00, 0.00, 0.00, 0.0064, 0.0113, 0.0371, 0.0678, 0.1037, 0.1427, 0.1736, 0.1793, 0.1636, 0.1280, 0.0780, 0.0440}; //r1017 *1.6
    //{1.00, 1.00, 1.00, 1.00, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000}; //best

    //{0.0, 0.0, 0.0, 0.001, 0.017, 0.101, 0.116, 0.124, 0.13, 0.139, 0.149, 0.159, 0.17, 0.18, 0.19, 0.2}; //r943-02
    //{0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.02, 0.065, 0.10, 0.138, 0.158, 0.169, 0.177, 0.185, 0.17, 0.09, 0.05}; //r8900
    //{0.0, 0.0, 0.0, 0.0, 0.001, 0.003, 0.07, 0.09, 0.13, 0.15, 0.16, 0.17, 0.18, 0.17, 0.09, 0.05}; //r7400U-20
    //4130nm,1550nm,1000nm,950nm,900nm,850nm, 800nm, 750nm,700nm,650nm, 600nm, 550nm, 500nm, 450nm,400nm,350nm

    //{0., 0., 0., 0., 0.00, 0.00, 0.0, 0.0, 0.01, 0.029, 0.055, 0.08, 0.10, 0.105, 0.108, 0.108}; //CPM 1993
    //{0., 0., 0., 0., 0.00, 0.00, 0.0, 0.0, 0.09, 0.261, 0.495, 0.72, 0.90, 0.945, 0.972, 0.972}; //CPM 1993*9
    //{0., 0., 0., 0., 0.001, 0.006, 0.015, 0.03, 0.047, 0.07, 0.088, 0.10, 0.101, 0.10, 0.09, 0.07}; //CPM 972P
    
    //{0., 0., 0., 0., 0.001, 0.15, 0.18, 0.20, 0.18, 0.18, 0.16, 0.15, 0.10, 0.08, 0.03, 0.1}; //H7421-50
    //{0.0, 0., 0., 0., 0., 0.000, 0.0, 0.0, 0.0, 0.08, 0.32, 0.38, 0.42, 0.42, 0.28, 0.12, 0.11}; //H7421-40
    
    G4double Photocath_REFL[OPT_NUMREAL] = {0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.};
    G4MaterialPropertiesTable* Photocath_mt = new G4MaterialPropertiesTable();
    Photocath_mt->AddProperty("EFFICIENCY",Ephoton,Photocath_EFF,OPT_NUMREAL);
    Photocath_mt->AddProperty("REFLECTIVITY",Ephoton,Photocath_REFL,OPT_NUMREAL);

    Photocath_surf->SetMaterialPropertiesTable(Photocath_mt);

    // dump material properties
    G4cout << *(G4Material::GetMaterialTable());

    return;
}

} // namespace lsr

