

Under `N_det_alt.dat` are simulation results from the 12.11.2015 with one PMT and with a tall ellipse. This simulation cannot be done again due to software changes.

In `N_det.dat` is data of the simulations from the last run with corresponds to the *.log files in this directory.
However, the z1 and z2 values in the parameter files were not changed and this is the reason that the data is not usable.
