/***********************************************************
 *
 * Parameters of the simulation of the ESR mirror system 
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * Modificated by Dominik Thomas
 * modification date: 13.10.2015
 *
 ***********************************************************/

/* Ion properties and transition */
beta    0.0    /* 0.71 */   /* ion velocity (v/c) */
tau     0.082  /* lifetime of the hyperfine state in the lab system [s] */
lambda  600   /* transition wavelength [nm] */
N_exc   2e5    /* average number of excited ions in the LSR */

/* LSR geometry */
s_lsr      108.36 /* circumference of LSR in [m] */
s_beam       5.0  /* straight section of beampipe simulated [m] */

beam_rad   125.0 /* beam tube radius [mm] */

detector_width       160.0 /* [mm] not used anymore */
detector_height     1200.0 /* [mm] not used anymore */
detector_length      250.0 /* [mm] See detector_length.png */
beamhole_radius       50.0 /* Required mininum distance from beamline [mm] */

mirror_section_type   2     /* 0 - FocusEllipse, Ctpfds, ThreeCtpfds */
mirror_section_height 450.0 /* [mm]  See focus_ellipse_height.png */
mirror_section_length 2000.0 

/* Compund Parabolic Concentrator (CPC) */
CPC_acceptanceAngle   30.0 /* acceptanceAngle [deg] */
CPC_areas             20   /* How many circles approx the CPC? */

/* viewport window [mm] */
vwp_wrad    36.0  /* radius of viewport windows    */
vwp_wthk     2.0  /* thickness of viewport windows */
vwp_height  72.0  /* height of viewport            */
vwp_frad    76.0  /* radius of viewport flange     */
vwp_fthk    20.0  /* thickness of viewport flange  */
