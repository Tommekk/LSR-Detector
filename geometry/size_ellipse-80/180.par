/***********************************************************
 *
 * Parameters of the simulation of the ESR mirror system 
 *
 * Author: V.M. Hannen
 * Modificated by Dominik Thomas
 * modification date: 19.04.2016
 *
 ***********************************************************/

/* Showcasemode */
showcasemode 0 /* 0 - Off, 1 - On */

/* Ion properties and transition */
beta    0.1     /* ion velocity (v/c) */
tau     0.082   /* lifetime of the hyperfine state in the lab system [s] */
lambda  0.0      /* transition wavelength [nm]
                     if 0.0,  specific wavelengths are picked
                     if -1.0, random wavelengths between 200 and 900 nm are picked */
N_exc    2e5    /* average number of excited ions in the LSR */
beam_dist  0    /* Distribution: 0 - Point, 1 - Gauss */
beam_width 5.0     /* half width [mm], only used when beam_dist is 1 (Gauss) */
beam_x     0.0  /* beam x position [mm] */
beam_y     -80.0  /* beam y position [mm] */
beam_z1   -0.25  /* Photons are created with z-values [m] from   */
beam_z2    0.25  /*     beam_z1 to beam_z2 (also for background) */
background_type 0 /* background type 0 - colinear laser, 1 - left laser, 2 - right laser, 3 - gas background */

/* LSR geometry */
s_lsr      54.17 /* circumference of LSR in [m] */
s_beam       1.0  /* straight section of beampipe simulated [m] */

beam_rad   125.0 /* beam tube radius [mm] */

detector_width       160.0 /* [mm] not used anymore */
detector_height     1200.0 /* [mm] not used anymore */
detector_length      250.0 /* [mm] See detector_length.png */
beamhole_radius       50.0 /* Required mininum distance from beamline [mm] */

mirror_section_type   0     /* 0 - FocusEllipse, Ctpfds, ThreeCtpfds */
mirror_section_height 180.0  /* [mm]  See focus_ellipse_height.png */
mirror_section_surface 2    /* Surface of mirror chamber, 2 - MIRO2, 90 - R90 */

/* Compund Parabolic Concentrator (CPC) */
CPC_areas             20   /* How many circles approx the CPC? */

tower_surface         90    /* Surface of CPC/Cone, 2 - MIRO2, 90 - R90 */

/* IF window (kodial) [mm] */
/* Type DN CF             16   40   40   50    63    63    100  160   200 (just enough place till 100) */
vwp_ifheight      72.0
vwp_ifwdia        89.0 /* 15   32   38   38    63    65    89   136   136 */
vwp_ifwthk         4   /*  1    3    3    3     3     3.5   4     8     8 */
vwp_ifwflangedia 152.4 /* 33.8 69.9 69.9 85.7 114.3 114.3 152.4 203.2 254 */
vwp_ifwflangethk  20.0 /* 12.7 12.7 12.7 16.0  17.4  17.4  20    22.3  24 */

vwp_iffocustype 0 /* 0 - CPC, 1 - lightguide, 2 - cone */

/* UV window (saphir) [mm] */
/* Type DN CF             16   40   40    63   100   160 (just enough place till 100)   */
vwp_uvheight      72.0
vwp_uvwdia        89.0 /* 15   32   38    63    89   136   */
vwp_uvwthk         2.5 /*  1.5  1.5  1.5   2     2.5   4   */
vwp_uvwflangedia 152.4 /* 33.8 69.9 69.9 114.3 152.4 203.2 */
vwp_uvwflangethk  20.0 /* 12.7 12.7 12.7  17.4  20    22.3 */

vwp_uvfocustype 0 /* 0 - CPC, 1 - lightguide, 2 - cone */

