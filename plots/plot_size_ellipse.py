import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.ticker import FuncFormatter


#Setting font to the same used in LaTeX
plt.rcParams.update({'font.size': 10})
rc('text', usetex=True)
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

def to_percent(y, position):
    # Ignore the passed in position. This has the effect of scaling the default
    # tick locations.
    s = str(int(round(100 * y)))
    return s + r'$\%$'


plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ellipse0 = np.loadtxt("../geometry/size_ellipse/N_det.dat")
ellipse25 = np.loadtxt("../geometry/size_ellipse25/N_det.dat")
ellipsem25 = np.loadtxt("../geometry/size_ellipse-25/N_det.dat")
ellipsem50 = np.loadtxt("../geometry/size_ellipse-50/N_det.dat")
ellipsem70 = np.loadtxt("../geometry/size_ellipse-70/N_det.dat")
ellipsem75 = np.loadtxt("../geometry/size_ellipse-75/N_det.dat")
ellipsem80 = np.loadtxt("../geometry/size_ellipse-80/N_det.dat")
# plot the first column as x, and second column as y

ellipse0_e = np.sqrt(ellipse0[:,1]);
ellipse25_e = np.sqrt(ellipse25[:,1]);
ellipsem25_e = np.sqrt(ellipsem25[:,1]);
ellipsem50_e = np.sqrt(ellipsem50[:,1]);
ellipsem70_e = np.sqrt(ellipsem70[:,1]);
ellipsem75_e = np.sqrt(ellipsem75[:,1]);
ellipsem80_e = np.sqrt(ellipsem80[:,1]);

ellipse0_e /= (1000000) # Get Percentage
ellipse25_e /= (1000000) # Get Percentage
ellipsem25_e /= (1000000) # Get Percentage
ellipsem50_e /= (1000000) # Get Percentage
ellipsem70_e /= (1000000) # Get Percentage
ellipsem75_e /= (1000000) # Get Percentage
ellipsem80_e /= (1000000) # Get Percentage

print(ellipsem70_e)


ellipse0[:,1] /= (1000000) # Get Percentage
ellipse25[:,1] /= (1000000) # Get Percentage
ellipsem25[:,1] /= (1000000) # Get Percentage
ellipsem50[:,1] /= (1000000) # Get Percentage
ellipsem70[:,1] /= (1000000) # Get Percentage
ellipsem75[:,1] /= (1000000) # Get Percentage
ellipsem80[:,1] /= (1000000) # Get Percentage


fig = plt.figure()
plt.errorbar(ellipse0[:,0], ellipse0[:,1], yerr=ellipse0_e, fmt='-o', label='  0 mm')
plt.errorbar(ellipse25[:,0], ellipse25[:,1], yerr=ellipse25_e, fmt='-o', label=' 25 mm')
#plt.errorbar(ellipsem25[:,0], ellipsem25[:,1], yerr=0.0031, fmt='-o', label='-25 mm')
plt.errorbar(ellipsem50[:,0], ellipsem50[:,1], yerr=ellipsem50_e, fmt='-o', label='-50 mm')
plt.errorbar(ellipsem70[:,0], ellipsem70[:,1], yerr=ellipsem70_e, fmt='-o', label='-70 mm')
plt.errorbar(ellipsem75[:,0], ellipsem75[:,1], yerr=ellipsem75_e, fmt='-o', label='-75 mm')
#plt.errorbar(ellipsem80[:,0], ellipsem80[:,1], yerr=0.0031, fmt='-o', label='-80 mm')
legend = plt.legend()
formatter = FuncFormatter(to_percent)

#shift = max([t.get_window_extent().width for t in legend.get_texts()])
for t in legend.get_texts():
    t.set_ha('right') # ha is alias for horizontalalignment
    t.set_position((40,0))

# Set the formatter
plt.gca().yaxis.set_major_formatter(formatter)
plt.xlabel(r"Halbachse $b$ [mm]")
plt.ylabel("Detektierte Beamphotonen")
plt.xlim(50.0, 210.)

fig.savefig("./pics/size_ellipse.pdf")
