# -*- coding: utf-8 -*- 
from __future__ import unicode_literals
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.ticker import FuncFormatter




#Setting font to the same used in LaTeX
plt.rcParams.update({'font.size': 10})
rc('text', usetex=True)
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})

def to_percent(y, position):
    # Ignore the passed in position. This has the effect of scaling the default
    # tick locations.
    s = str(100 * y)
    return s + r'$\%$'



#plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
#data = np.genfromtxt("../geometry/CPCorCone/N_det.dat", dtype=None)
# plot the first column as x, and second column as y

N = 2
photons = (108821, 117785)
background = (8, 3)

photons_perc = tuple([x/1000000.0 for x in photons])

ind = np.arange(N)  # the x locations for the groups
width = 0.2       # the width of the bars

fig, ax = plt.subplots()


my_xticks = ['Kegelstumpf', 'CPC']
rects1 = ax.bar(ind, photons_perc, color='r', yerr=0.003, ecolor='m', align='center')

formatter = FuncFormatter(to_percent)

# Set the formatter
plt.gca().yaxis.set_major_formatter(formatter)

ax.set_ylabel('Detektierte Photonen')
ax.set_title(r"Sekund{\"a}rspiegel")
ax.set_xticks(ind)
ax.set_xticklabels(my_xticks)



fig.savefig("./pics/CPCorCone.png")
