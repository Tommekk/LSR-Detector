# Plots to show probability, that photons comes into detector


set terminal png

set samples 101

set title 'Wahrscheinlichkeit, dass Photon in die Detektorkammer gelangt'
set ylabel 'Wahrscheinlichkeit'
set grid
#set xtics 50
set yrange [0:1]
set xrange [-25:25]


set output 'probability_photon_moves_in_detector.png'
set xlabel 'Abstand vom Detektor [cm]'

l = 50
r = 5

#f(x) = 1/2*( (l+x)/sqrt((l+x)*(l+x) + r*r) - 1/sqrt(1+r*r/(x*x) ))
f(x) = 1/2*( (25+x)/sqrt((25+x)*(25+x) + 25) - 1/sqrt(1+25/(x*x) ))
plot [0:20] 5 notitle

set output 'probability_photon_moves_out_detector.png'

set title 'Wahrscheinlichkeit, dass Photon die Detektorkammer verlässt'
set xlabel 'Abstand vom Mittelpunkt des Detektors [cm]'

l_2 = l/2

#g(x) = 1-1/2*( (l_2+x)/sqrt((l_2+x)*(l_2+x) + r*r) + (l_2-x)/sqrt((l_2-x)*(l_2-x) + r*r))
g(x) = 1-1/2*(((25+x)/sqrt((25+x)*(25+x) + 25)) + (25-x)/sqrt((25-x)*(25-x) + 25))
plot [-25:25] g(x) notitle
