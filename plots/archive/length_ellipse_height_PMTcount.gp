set terminal png


set title 'Detected photons of 20 Mio. start photons '
set ylabel 'Detected photons'
set grid
#set xtics 50


set output 'detector_length.png'
set xlabel 'Detector length [mm]'
plot '../geometry/detector_length/N_det.dat'      using 1:2:(sqrt($2))  with yerrorbars notitle

set output 'focus_ellipse_height.png'
set xlabel 'Ellipse height [mm]'
plot [130:820] '../geometry/focus_ellipse_height/N_det.dat' using 1:2:(sqrt($2))  with yerrorbars notitle

set output 'PMT_number.png'
set xlabel 'PMT count'
plot [0:3.5] '../geometry/PMT_number/N_det.dat' using 1:2:(sqrt($2))  with yerrorbars notitle
