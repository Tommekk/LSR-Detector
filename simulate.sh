#!/bin/bash
#geofilesdir= 'geometry\/focus_ellipse_height'
for i; do # ../geometry/test/*.par
  echo "$(date '+%d/%m/%Y %H:%M:%S'): Simulate $i"
  cp -- "$i" "geometry.par";
  { time ./lsr_detector run.mac; } > "$i.log" 
  mv -- "lsr_histo.root" "$i.root" 
done
echo "$(date '+%d/%m/%Y %H:%M:%S'): Ende"
