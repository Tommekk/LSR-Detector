#!/bin/bash
echo 'Starting Cpplint...'
./cpplint.py --counting=detailed  $( find src/ include/  -name \*.h -or -name \*.cc -not -path "./directory/*")
echo 'Starting Cppcheck...'
cppcheck --enable=all src include -isrc/myutils.cc -isrc/_esr_SegmentedMirrorConstruction.cpp 
