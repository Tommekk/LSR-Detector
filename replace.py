import glob, os


os.chdir("/home/tommekk/LSR-Detector/geometry/CPCorCone/")
for filename in glob.glob("*.par"):

	file = open(filename, "r")
	background_type = "background_type 0 /* background type 0 - colinear laser, 1 - left laser, 2 - right laser, 3 - gas background */\n"
	mirror_section_surface = "mirror_section_surface 2    /* Surface of mirror chamber, 2 - MIRO2, 90 - R90 */\n"
	tower_surface = "tower_surface         90    /* Surface of CPC/Cone, 2 - MIRO2, 90 - R90 */\n"
	string = "" 

	for line in file:
	
		if line.startswith("beam_z2"):  
			string = string + line + background_type
		else:
			string = string + line
        """
		elif line.startswith("mirror_section_height"):  
			string = string + line + mirror_section_surface
		elif line.startswith("CPC_areas"):  
			string = string + line + tower_surface
        
		"""
		

	file.close()
    
	print string
	
	file_new = open(filename, "w")
	file_new.write("%s" % string)
	
