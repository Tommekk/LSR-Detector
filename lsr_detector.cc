/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modification date: 18.2.09
 *
 ***********************************************************/
#include "Constants.hh"
#include "lsr/DetectorConstruction.hh"
#include "lsr/PhysicsList.hh"
#include "lsr/PrimaryGeneratorAction.hh"
#include "lsr/SteppingAction.hh"
#include "lsr/RunAction.hh"
#include "lsr/HistoManager.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"


// Definition of global structures
transition_par_t * tpar;
geometry_par_t * gpar;

char BASEDIR[255];


// function prototypes
extern int getpar (const char * fname);


int main(int argc,char** argv)
{
    // Detect interactive mode (if no arguments) and define UI session
    G4UIExecutive* ui = 0;
    if ( argc == 1 ) ui = new G4UIExecutive(argc, argv);

    // set simulation basedir
    sprintf(BASEDIR, "./");

    // read simulation parameters
    if (getpar ("geometry.par") < 0) return -1;

    // Construct the default run manager
    G4RunManager* runManager = new G4RunManager;

    // set mandatory initialization classes
    runManager->SetUserInitialization(&lsr::DetectorConstruction::getInstance());
    runManager->SetUserInitialization(new lsr::PhysicsList());

    // initialize histoManager
    lsr::HistoManager * histo = new lsr::HistoManager();

    // set mandatory user action class
    runManager->SetUserAction(new lsr::PrimaryGeneratorAction(histo));

    // set user action class
    runManager->SetUserAction(new lsr::RunAction(histo));
    runManager->SetUserAction(new lsr::SteppingAction(histo));

    // Initialize G4 kernel
    runManager->Initialize();

    // Initialize visualization
    //
    G4VisManager* visManager = new G4VisExecutive;
    // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
    // G4VisManager* visManager = new G4VisExecutive("Quiet");
    visManager->Initialize();

    // Get the pointer to the User Interface manager
    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    // Process macro or start UI session
    //
    if ( ! ui )
    {
        // batch mode
        G4String command = "/control/execute ";
        G4String fileName = argv[1];
        UImanager->ApplyCommand(command+fileName);
    }
    else
    {
        // interactive mode
        UImanager->ApplyCommand("/control/execute init_vis.mac");
        ui->SessionStart();
        delete ui;
    }

    // Job termination
    // Free the store: user actions, physics_list and detector_description are
    // owned and deleted by the run manager, so they should not be deleted
    // in the main() program !


    return 0;
}


