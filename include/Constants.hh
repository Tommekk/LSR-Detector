/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modificated by: Dominik Thomas
 * modification date: 19.06.2016
 *
 ***********************************************************/
#ifndef lsrCONSTANTS_H          /* Use only once if included recursively */
#define lsrCONSTANTS_H 1

#include "G4SystemOfUnits.hh"

#include "G4ThreeVector.hh"
#include "globals.hh"

// Natural constants
#define h_eV   4.1356e-15  // Planck's constant in eV
#define clight 299792458.0 // Speed of light [m/s]

// Ion properties and transition
typedef struct { 
    double beta;         // ion velocity (v/c)
    double tau;          // lifetime of the hyperfine state [s]
    double lambda;       // transition wavelength [nm]
    double N_exc;        // average number of excited ions in the lsr
    int    beam_dist;    // beam dstribution: 0 - Point, 1 - Gauss
    double beam_width;   // half width [mm] only used when beam_dist is 1 (Gauss)
    double beam_x;       // beam x position [mm]
    double beam_y;       // beam y position [mm]
    double beam_z1;      // Photons are created with z-values [m] from
    double beam_z2;      //   beam_z1 to beam_z2 (also for background)
    int    backgr_type;  // background type 0 - colinear laser, 1 - left laser, 2 - right laser, 3 - gas background
} transition_par_t;

extern transition_par_t * tpar;

/*******************************
 * Optics and detector geometry
 *******************************/
typedef struct { 
    int showcasemode;

    // lsr geometry
    double s_lsr;        // circumference of lsr in [m]
    double s_beam;       // straight section of beampipe simulated [m]
    double beam_rad;     // beam tube radius [mm]

    double detector_width;  /* [mm] */
    double detector_height; /* [mm] */
    double detector_length; /* [mm] */
    double beamhole_radius; /* Required mininum distance from beamline [mm] */
    int mirror_section_type; /* Type of MirrorSection
                                   0 - FocusEllipse,
                                   1 - Ctpfds,
                                   2 - ThreeCtpfds */
    double mirror_section_height; /* [mm] */
    int mirror_section_surf; /* Surface of mirror chamber, 2 - MIRO2, 90 - R90 */

    /* Compund Parabolic Concentrator (CPC) */
    int CPC_areas;              // How many circles approx the CPC?
    int tower_surf; /* Surface of CPC/Cone, 2 - MIRO2, 90 - R90 */

    // IF window (kodial) [mm]
    double vwp_ifheight;     // height
    double vwp_ifwrad;       // radius [mm]
    double vwp_ifwthk;       // window thickness [mm]
    double vwp_ifwflangerad; // flange radius
    double vwp_ifwflangethk; // flange height [mm]
    int    vwp_iffocustype;  // 0 - CPC, 1 - lightguide, 2 - cone

    // UV window (saphir) [mm]
    double vwp_uvheight;     // height
    double vwp_uvwrad;       // radius [mm]
    double vwp_uvwthk;       // window thickness [mm]
    double vwp_uvwflangerad; // flange radius
    double vwp_uvwflangethk; // flange height [mm]
    int    vwp_uvfocustype;  // 0 - CPC, 1 - lightguide, 2 - cone




} geometry_par_t;

extern geometry_par_t * gpar;

#endif /* lsrCONSTANTS_H */
