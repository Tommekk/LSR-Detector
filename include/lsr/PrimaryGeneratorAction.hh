/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modificated by: Dominik Thomas
 * modification date: 19.06.2016
 *
 ***********************************************************/

#ifndef lsr_PrimaryGeneratorAction_h
#define lsr_PrimaryGeneratorAction_h 1

#include "lsr/HistoManager.hh"

#include "G4VUserPrimaryGeneratorAction.hh"

#include <TRandom3.h>


class G4ParticleGun;
class G4Event;

namespace lsr {

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
    PrimaryGeneratorAction(HistoManager* hstMngr);
    ~PrimaryGeneratorAction();

public:
    void GeneratePrimaries(G4Event* anEvent);

private:
    void GenerateBeamParticle(G4Event* anEvent);
    void GenerateGasBackgroundParticle(G4Event* anEvent);
    void GenerateLaserBackgroundParticle(G4Event* anEvent);
    G4double CMSAngleToLabAngle(double theta, double beta);


    G4ParticleGun* particleGun;

    double gamma;   // Lorentz factor
    double tau_lab; // lifetime of the hyperfine state [s]

    // Random number generator
    TRandom3 *dice;

    HistoManager* histoManager;
};

} // namespace lsr

#endif
