/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modificated by: Dominik Thomas
 * modification date: 19.06.2016
 *
 ***********************************************************/

#ifndef lsr_PMTHisto_h
#define lsr_PMTHisto_h 1

#include <string>

#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TTree.h>
#include <G4ThreeVector.hh>

namespace lsr {

class PMTHisto
{
public:
    PMTHisto(G4int count);
    ~PMTHisto();

    void addToTree(TTree* tree);
    void Clear();
    void Write();


    // PMTHisto of segmented mirror section
    TH3F *hdetpos;   // 3D-Positions of detected photons by this PMT

    TH1F *hbeamlambdadet; // wavelength of detected photons

    TH1F *hbeamsegthdet;    // polar angles incident on the photocathode of detected beam photons
    TH1F *hbeamsegz0det;    // z0-coordinate of detected beam photons
    TH2F *hbeamsegstartxyd; // x-y-coordinates of starting point of detected beam photons
    TH2F *hbeamsegstartxzd; // xz-plot coodinate plot of start positon of beam photon in chamber
    TH2F *hbeamsegxzd;      // xz-plot coordinate plot of beam photon position on cathode

    TH1F *hbackgroundsegthdet;    // polar angles incident on the photocathode of detected background photons
    TH1F *hbackgroundsegz0det;    // z0-coordinate of detected background photons
    TH2F *hbackgroundsegstartxyd; // x-y-coordinates of starting point of detected background photons
    TH2F *hbackgroundsegstartxzd; // xz-plot coodinate plot of start positon of background photon in chamber
    TH2F *hbackgroundsegxzd;      // xz-plot coordinate plot of background photon position on cathode

private:
    G4int id;

    std::string makeName(const char* name);

};

} // namespace lsr

#endif
