/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modification date: 18.2.09
 *
 ***********************************************************/
#ifndef lsr_Run_h
#define lsr_Run_h 1

#include "globals.hh"
#include "G4Run.hh"

class G4Event;

namespace lsr {

class Run : public G4Run
{
public:
    Run();
    virtual ~Run();

    virtual void RecordEvent(const G4Event*);

private:

};

} // namespace lsr

#endif
