/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modification date: 18.2.09
 *
 ***********************************************************/
#ifndef lsr_RunAction_h
#define lsr_RunAction_h 1

#include "lsr/HistoManager.hh"

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;

namespace lsr {

class RunAction : public G4UserRunAction
{
public:
    RunAction(HistoManager* hstMngr);
    ~RunAction();

public:
    G4Run* GenerateRun();
    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);


private:
    HistoManager* histoManager;
};

} // namespace lsr

#endif

