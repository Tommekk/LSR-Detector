/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modification date: 18.2.09
 *
 ***********************************************************/

#ifndef lsr_PhysicsList_h
#define lsr_PhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"

#include "G4OpAbsorption.hh"
#include "G4OpRayleigh.hh"
#include "G4OpBoundaryProcess.hh"

namespace lsr {

class PhysicsList: public G4VUserPhysicsList
{
public:
    PhysicsList();
    ~PhysicsList();

protected:
    // Construct particle and physics
    void ConstructParticle();
    void ConstructProcess();

    void SetCuts();

    G4OpAbsorption*      theAbsorptionProcess;
    G4OpRayleigh*        theRayleighScattering;
    G4OpBoundaryProcess* theBoundaryProcess;
};

} // namespace lsr

#endif
