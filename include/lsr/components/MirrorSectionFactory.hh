/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/


#ifndef lsr_MirrorSectionFactory_h
#define lsr_MirrorSectionFactory_h 1

#include "MirrorSection.hh"
#include "globals.hh"

#include "G4ThreeVector.hh"
#include "G4VSolid.hh"

namespace lsr {
namespace components {

class MirrorSectionFactory
{
public:
    static MirrorSection* createMirrorSection(const std::string &description);
    static MirrorSection* createMirrorSection(int id);

};

} // namespace components
} // namespace lsr

#endif
