/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#ifndef lsr_Ctpfds_h
#define lsr_Ctpfds_h 1

#include "globals.hh"
#include "MirrorSection.hh"


#include "G4ThreeVector.hh"
#include "G4VSolid.hh"

namespace lsr {
namespace components {

class Ctpfds : public MirrorSection
{
public:
    Ctpfds();
    ~Ctpfds();

    G4double getHeight() const;
    G4double getWidth() const;
    G4double getEccentricity() const;
    G4ThreeVector getPosition() const;
    bool isPointWithin(G4double x, G4double y) const;
    G4VSolid* getSolid();
    G4double getCathodeHeight() const;
private:
    G4double height;
    G4double width;

    G4double ellipse_height;
    G4double ellipse_width;
    G4double ellipse_eccentricity;

};

} // namespace components
} // namespace lsr

#endif
