/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#ifndef lsr_Aperture_h
#define lsr_Aperture_h 1

#include "globals.hh"
#include "G4Tubs.hh"


namespace lsr {
namespace components {

class Aperture {

public:
    Aperture();


    G4VSolid* getSolid(const G4String &pName);
private:
};

} // namespace components
} // namespace lsr

#endif
