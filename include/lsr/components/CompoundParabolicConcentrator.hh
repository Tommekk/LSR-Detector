/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#ifndef lsr_CompoundParabolicConcentrator_h
#define lsr_CompoundParabolicConcentrator_h 1

#include "globals.hh"
#include "G4Polycone.hh"

namespace lsr {
namespace components {

class CompoundParabolicConcentrator {

public:
    CompoundParabolicConcentrator();

    //static double getEntryRadius(double r_exit, double acceptance_angle);
    double getEntryRadius() const;

    //static double getHeight(double r_exit, double acceptance_angle)
    double getBottomHeight() const;

    G4Polycone* getG4Polycone(const G4String &pName);
    double getLength() const;
private:
    G4double rExit, rEntry, acceptanceAngle;




    double getF() const;
};

} // namespace components
} // namespace lsr

#endif
