/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#ifndef lsr_Chamber_h
#define lsr_Chamber_h 1

#include "lsr/components/FocusEllipse.hh"
#include "lsr/components/MirrorSection.hh"

#include "globals.hh"

#include "G4ThreeVector.hh"

namespace lsr {
namespace components {

class Chamber
{
public:
    Chamber(lsr::components::MirrorSection* mirror_section);
    ~Chamber();

    G4double getWidth() const;
    G4double getHeight() const;
    G4double getLength() const;
    G4ThreeVector getPosition() const;



    G4double getBeamholeRadius() const;
    G4double getBeamholeLength() const;

private:

    G4double width;
    G4double height;
    G4double length;
    G4ThreeVector position;  // Position of the complete detector setup

    G4double beamhole_radius;
    G4double beamhole_length;


};

} // namespace components
} // namespace lsr

#endif
