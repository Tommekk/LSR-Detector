/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#ifndef lsr_IFViewportWindow_h
#define lsr_IFViewportWindow_h 1


#include "ViewportWindow.hh"

#include "globals.hh"


namespace lsr {
namespace components {

class IFViewportWindow : public ViewportWindow
{
public:
    IFViewportWindow();
    ~IFViewportWindow();

    G4double getWindowRadius() const;
    G4double getWindowThickness() const;
    G4double getHeight() const;
    G4double getFlangeRadius() const;
    G4double getFlangeThickness() const;
    G4VSolid* getSolid();

private:
    //----------------------
    // viewport window (at the top)
    //----------------------
    G4double window_radius;    // radius of viewport windows
    G4double window_thickness;    // thickness of viewport windows
    G4double height;  // height of viewport
    G4double flange_radius;    // radius of viewport flange
    G4double flange_thickness;    // thickness of viewport flange

};

} // namespace components
} // namespace lsr

#endif
