/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#ifndef lsr_MirrorSection_h
#define lsr_MirrorSection_h 1

#include "globals.hh"

#include "G4ThreeVector.hh"
#include "G4VSolid.hh"

namespace lsr {
namespace components {

class MirrorSection
{
public:

    virtual G4double getHeight() const = 0;
    virtual G4double getWidth() const = 0;
    virtual G4double getEccentricity() const = 0; // distance from center of solid to focus point
    virtual G4ThreeVector getPosition() const = 0;
    virtual G4VSolid* getSolid() = 0;
    virtual bool isPointWithin(G4double x, G4double y) const = 0;
    virtual G4double getCathodeHeight() const = 0;  // should be removed

};

} // namespace components
} // namespace lsr

#endif
