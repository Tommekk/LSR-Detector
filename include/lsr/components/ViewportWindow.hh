/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#ifndef lsr_ViewportWindow_h
#define lsr_ViewportWindow_h 1

#include "globals.hh"
#include "G4VSolid.hh"

namespace lsr {
namespace components {

class ViewportWindow
{
public:
    virtual G4double getWindowRadius() const = 0;
    virtual G4double getWindowThickness() const = 0;
    virtual G4double getHeight() const = 0;
    virtual G4double getFlangeRadius() const = 0;
    virtual G4double getFlangeThickness() const = 0;
    virtual G4VSolid* getSolid() = 0;
};

} // namespace components
} // namespace lsr

#endif
