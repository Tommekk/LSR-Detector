/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modificated by: Dominik Thomas
 * modification date: 19.06.2016
 *
 ***********************************************************/
#ifndef lsr_DetectorConstruction_h
#define lsr_DetectorConstruction_h 1


class G4LogicalVolume;
class G4VPhysicalVolume;

#include "G4Material.hh"
#include "G4OpticalSurface.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4SubtractionSolid.hh"

#include "lsr/components/Chamber.hh"
#include "lsr/components/CompoundParabolicConcentrator.hh"
#include "lsr/components/MirrorSection.hh"
#include "lsr/components/IFViewportWindow.hh"
#include "lsr/components/UVViewportWindow.hh"


namespace lsr {

class DetectorConstruction : public ::G4VUserDetectorConstruction
{
public:
    ~DetectorConstruction();

    static DetectorConstruction& getInstance();

    // don't allow copying (C++11)
    DetectorConstruction(DetectorConstruction const&) = delete;
    void operator=(DetectorConstruction const &)      = delete;

    G4VPhysicalVolume* Construct();

    components::MirrorSection *getMirrorSection() const;

    components::ViewportWindow *getViewportWindow() const;

    void printStatistic() const;

    components::IFViewportWindow *getIFViewportWindow() const;

    components::UVViewportWindow *getUVViewportWindow() const;


private:
    DetectorConstruction();

    // Function prototypes
    void DefineMaterials();
    void BuildParabolicMirror();
    void BuildMirrorSection();
    void BuildEllipticMirror();
    void BuildForwardConstruction();
    void PlaceDetectors();
    void MakeChamberHoles();

    void placeUVDetector(G4double cathode_height, G4double zpos, G4int number);
    void placeIFDetector(G4double cathode_height, G4double zpos, G4int number);

    double CalcTransmissionProbabilityToAbsorptionLength(double percentage, double glasstickness);
    G4OpticalSurface* getMaterial(int material_number);




    // Materials
    G4Material* Air;
    G4Material* Vacuum;
    G4Material* Glass;
    G4Material* PGlass;
    G4Material* Steel;
    G4Material* Kodial;
    G4Material* Saphir;

    // Surfaces
    G4OpticalSurface* Mirror_surf;
    G4OpticalSurface* MIRO2_surf;
    G4OpticalSurface* Kodial_surf;
    G4OpticalSurface* Saphir_surf;


    G4OpticalSurface* Photocath_surf;
    G4OpticalSurface* Pipe_surf;
    G4OpticalSurface* R90_surf;

    // Solid volumes
    G4VSolid* detector_solid;

    // Logical volumes
    G4LogicalVolume* experimentalHall_log;
    G4LogicalVolume* beamtube_log;

    G4LogicalVolume* detector_log;
    G4LogicalVolume* lightguide_log;
    G4LogicalVolume* ifviewportwindow_log;
    G4LogicalVolume* uvviewportwindow_log;
    G4LogicalVolume* retub_log;
    G4LogicalVolume* spmt_log;
    G4LogicalVolume* scath_log;
    G4LogicalVolume* CPC_log;
    G4LogicalVolume* cone_log;
    G4LogicalVolume* aperture_log;

    // Physical volumes
    G4VPhysicalVolume* experimentalHall_phys;
    G4VPhysicalVolume* beamtube_phys;

    G4VPhysicalVolume* mirror_phys;
    G4VPhysicalVolume* window_phys;
    G4VPhysicalVolume* flange_phys;
    G4VPhysicalVolume* lg_phys;
    G4VPhysicalVolume* lense_phys;
    G4VPhysicalVolume* ppmt_phys;
    G4VPhysicalVolume* pcath_phys;

    //----------------------------------
    // detector setups
    //----------------------------------

    components::MirrorSection* mirror_section;  // MirrorSection component (FocusEllipse and so on)
    components::Chamber*       chamber;         // Chamber for FocusEllipse
    components::IFViewportWindow* ifvwpwindow; // IFViewport(s)
    components::UVViewportWindow* uvvwpwindow; // UVViewport
    components::CompoundParabolicConcentrator* CPC; // CPC-Creator class

    double detector_distance;         // distance between two CPCs and PMTs
    double detector_first_z_position; // z-position of the first CPC and PMT
    const double detector_count = 3;            // amount of CPCs and PMTs used

    double hight_lg; // half lightguide height
    double rad_det;  // radius photocathode's for mirror section (between spmt and lightguide)

    bool showcasemode = false;
};

} // namespace lsr

#endif // lsr_DetectorConstruction_H
