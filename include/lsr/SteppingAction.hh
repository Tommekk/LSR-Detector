/***********************************************************
 *
 * Simulation of lsr mirror system
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modification date: 18.2.09
 *
 ***********************************************************/

#ifndef lsr_SteppingAction_h
#define lsr_SteppingACtion_h 1

#include "lsr/HistoManager.hh"

#include "G4UserSteppingAction.hh"
#include "globals.hh"

namespace lsr {

class SteppingAction : public G4UserSteppingAction
{
public:
    SteppingAction(HistoManager* hstMngr);
    ~SteppingAction();
    virtual void UserSteppingAction(const G4Step*);

private:
    HistoManager* histoManager;
};

} // namespace lsr

#endif
