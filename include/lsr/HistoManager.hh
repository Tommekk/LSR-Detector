/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/


#ifndef HistoManager_h
#define HistoManager_h 1

#include "globals.hh"

#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TH3F.h>
#include <CLHEP/Units/SystemOfUnits.h>

#include "PMTHisto.hh"

namespace lsr {

const G4int MAXHISTO = 3; // Max PMT number %should be the same to the PMT Num in geometry.par

class HistoManager
{
public:

    HistoManager();
    ~HistoManager();

    void book();
    void save();

    PMTHisto* getPMTHisto(G4int id);
    
    void PrintStatistic();

    TH3F *halldetpos;  // positions of all detected photons

    // histograms
    // beam photon
    TH1F *hbeamtheta;         // polar emission angle
    TH1F *hbeamlambdacms;     // wavelength in cms
    TH1F *hbeamlambda;        // wavelength in lab system
    TH1F *hbeamphi;           // azimuthal emission angle
    TH1F *hbeamphidet;        // azimuthal emission angle detected
    TH1F *hbeamthetacathode;  // theta angles incident on the photocathode
    TH1F *hbeamthetadet;  // start angle of detected photons
    TH1F *hbeamlambdadet;     // wavelength of detected photons
    TH1F *hbeamzdet;          // start z position of detected photons


    // background photon
    TH1F *hbackgroundtheta;     // polar emission angle
    TH1F *hbackgroundthetadet;  // start angle of detected photons
    TH1F *hbackgroundlambda;    // wavelength
    TH1F *hbackgroundphi;       // azimuthal emission angle
    TH1F *hbackgroundphidet;    // azimuthal emission angle detected
    TH1F *hbackgroundthdet;     // theta angles incident on the photocathode
    TH1F *hbackgroundlambdadet; // detected wavelength
    TH1F *hbackgroundzdet;      // start z position of detected photons

    // variables
    double N_req;   // requested number of photons to produce
    double N_tot;   // total number of photons produced
    double N_det;   // total number of photons detected

    double N_emit;  // Number of emitted photons per second

    double N_beam_tot;  // total number of beam photons produced
    double N_beam_det;  // total number of beam photons detected

    double N_beam_chamber_tot; // total number of beam photons produced in chamber
    double N_beam_chamber_det; // total number of beam photons from chamber detected

    double N_background_tot;  // total number of background photons produced
    double N_background_det;  // total number of background photons detected

    double theta;
    double azimuth;
    double lambda;

    G4ThreeVector r0;          // start position
    bool isBackgroundParticle; // started as backgoround particle

private:

    TFile*    fRootFile;
    PMTHisto* fHisto[MAXHISTO];


};

} // namespace lsr

#endif

