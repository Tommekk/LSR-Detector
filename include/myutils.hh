/*			myutils.h
 *
 * Useful routines.
 *
 */
#ifndef MYUTILS          /* Use only once if included recursively */
#define MYUTILS

#include <stdio.h>

typedef int8_t    sint8;
typedef int16_t   sint16;
typedef int32_t   sint32;
typedef int64_t   sint64;
typedef u_int8_t  uint8;
typedef u_int16_t uint16;
typedef u_int32_t uint32;
typedef u_int64_t uint64;
typedef sint64    longestsint;
typedef uint64    longestuint;

extern char BASEDIR[255];

int get_string  (FILE * file, char * text);
int get_keyword (FILE * file, const char * keyword);
int get_int     (FILE * file, int * value, int min, int max);
int get_float   (FILE * file, float * value, float min, float max);
int get_double  (FILE * file, double * value, double min, double max);
int get_long    (FILE * file, long * value, long min, long max);
int get_uint16  (FILE * file, uint16 * value, uint16 min, uint16 max);
int get_uint32  (FILE * file, uint32 * value, uint32 min, uint32 max);
int get_hex16   (FILE * file, uint16 * value, uint16 min, uint16 max);
int get_hex32   (FILE * file, uint32 * value, uint32 min, uint32 max);
int get_binary  (FILE * file, uint16 * value, int no_bits);

int fequal (float a, float b);
FILE * fileopen (const char * fname, const char * type);

int kbhit(char * c, int sleeptime);
int kbhit_rawtt(char * c, int sleeptime);

void bitpattern (unsigned value);
unsigned getbits (unsigned x, int p, int n);

void hi_txt();
void lo_txt();
void cl_term();
void home_term();
void put_str(int m, int n, char s[80], int i);

#endif /* MYUTILS */







