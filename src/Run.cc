/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/Run.hh"
#include "G4Event.hh"

namespace lsr {

Run::Run() {}


Run::~Run() {}


void Run::RecordEvent(const G4Event*) {
    numberOfEvent++;
}

}  // namespace lsr

