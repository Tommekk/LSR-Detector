/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/components/ThreeCtpfds.hh"
#include "lsr/DetectorConstruction.hh"
#include "lsr/components/IFViewportWindow.hh"

#include "Constants.hh"

#include "G4ThreeVector.hh"
#include "G4EllipticalTube.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4IntersectionSolid.hh"
#include "G4Orb.hh"
#include "G4UnionSolid.hh"


namespace lsr {
namespace components {

/**
 * @brief ThreeCtpfds::ThreeCtpfds
 */
ThreeCtpfds::ThreeCtpfds() {
    width = 150*mm;

    height = 100*mm;
}

ThreeCtpfds::~ThreeCtpfds() {}

/**
 * @brief ThreeCtpfds::getHeight
 * @return half height in mm
 */
G4double ThreeCtpfds::getHeight() const {  // should be changed to real height
    return height;
}

/**
 * @brief ThreeCtpfds::getWidth
 * @return half width in mm
 */
G4double ThreeCtpfds::getWidth() const {
    return width;
}

/**
 * @brief ThreeCtpfds::getEccentricity
 * @return eccentricity of the ellipse in mm
 */
G4double ThreeCtpfds::getEccentricity() const {
    // linear eccentricity
    // distance between one focal point and the middle of the ellipse
    return 0;  // should not be used anymore
}

G4ThreeVector ThreeCtpfds::getPosition() const {
    return G4ThreeVector(0, 0, 0);
}

bool ThreeCtpfds::isPointWithin(G4double x, G4double y) const {
    return (x*x+y*y < 5*cm) ? true : false;  // should be more specific and depend on radius
}

G4VSolid* ThreeCtpfds::getSolid() {
    // CONDENSING-TYPE PORTABLE
    // FLUORESCENCE DETECTION SYSTEM
    //------------------------
    // concentrators
    //------------------------
    G4double beam_rad  = 50*mm;

    // lower material half
    G4double low_rad  = 100*mm;

    G4Orb* low_orb = new G4Orb("low_orb", low_rad);

    // upper material half
    G4double up_height = 75*mm;
    G4double up_rad    = 86*mm;

    G4Orb* up_orb = new G4Orb("up_orb", up_rad);
    G4IntersectionSolid* ctpfds =
            new G4IntersectionSolid("ctpfds",
                             up_orb,
                             low_orb,
                             G4TranslateY3D(-up_height/2 - (beam_rad+10*mm)/2
                                            +(beam_rad+10*mm)/2+low_rad/2
                                            +up_height/2));
    // Make three
    G4double distance =  250.0*mm / 3*2;  // should be dependable on chamber length

    G4UnionSolid* two_ctpfds =
            new G4UnionSolid("two_ctpfds",
                             ctpfds,
                             ctpfds,
                             G4TranslateZ3D(distance));
    G4UnionSolid* three_ctpfds =
            new G4UnionSolid("three_ctpfds",
                             two_ctpfds,
                             ctpfds,
                             G4TranslateZ3D(-distance));
    return three_ctpfds;
}

G4double ThreeCtpfds::getCathodeHeight() const {
    // Calculates the Height, where to put the Viewport Window
    // with sqrt it is calculated at which height of the ellipse it has the radius
    // of the viewport window radius
    ViewportWindow* viewport = DetectorConstruction::getInstance().getIFViewportWindow();

    return height + viewport->getWindowThickness()/2;
}

}  // namespace components
}  // namespace lsr

