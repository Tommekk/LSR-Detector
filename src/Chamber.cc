/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "Constants.hh"

#include "lsr/components/Chamber.hh"
#include "lsr/components/FocusEllipse.hh"
#include "lsr/DetectorConstruction.hh"

#include "G4SystemOfUnits.hh"


namespace lsr {
namespace components {

/**
 * @brief Chamber::Chamber
 */
Chamber::Chamber(MirrorSection *mirror_section) {
    //----------------------------------
    // chamber setups
    //----------------------------------
    // New Michael Hammen-like mirror
    length      = gpar->detector_length*mm;

    beamhole_length      = length + 5*mm;
    beamhole_radius      = gpar->beamhole_radius*mm;

    width       = mirror_section->getWidth()+10*mm;
    height      = 2*mirror_section->getHeight() - beamhole_radius + 10*mm;
    position = G4ThreeVector(0, 0, 0);  // Position of the complete detector setup
}

Chamber::~Chamber() {}

G4double Chamber::getLength() const {
    return length;
}

G4ThreeVector Chamber::getPosition() const {
    return position;
}

G4double Chamber::getBeamholeRadius() const {
    return beamhole_radius;
}

G4double Chamber::getBeamholeLength() const {
    return beamhole_length;
}

G4double Chamber::getWidth() const {
    return width;
}

G4double Chamber::getHeight() const {
    return height;
}

}  // namespace components
}  // namespace lsr

