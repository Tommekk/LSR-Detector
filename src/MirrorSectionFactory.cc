/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/components/MirrorSectionFactory.hh"

#include "Constants.hh"

#include "lsr/components/FocusEllipse.hh"
#include "lsr/components/Ctpfds.hh"
#include "lsr/components/ThreeCtpfds.hh"
#include "lsr/components/Circle.hh"


namespace lsr {
namespace components {

MirrorSection *MirrorSectionFactory::createMirrorSection(const std::string &description) {
    if (description == "FocusEllipse")
        return createMirrorSection(0);
    if (description == "Ctpfds")
        return createMirrorSection(1);
    if (description == "ThreeCtpfds")
        return createMirrorSection(2);
    if (description == "Circle") {
        return createMirrorSection(3);
    }
    return NULL;
}

MirrorSection *MirrorSectionFactory::createMirrorSection(int id) {
    switch (id) {
    case 0:
        return new FocusEllipse();
    case 1:
        return new Ctpfds();
    case 2:
        return new ThreeCtpfds();
    case 3:
        return new Circle();
    default:
        return NULL;
    }
}


}  // namespace components
}  // namespace lsr

