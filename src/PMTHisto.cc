/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/PMTHisto.hh"

#include <string>
#include <TFile.h>

#include "Constants.hh"



namespace lsr {

PMTHisto::PMTHisto(G4int count) {
    this->id = count;

    // initialize histograms
    hdetpos =
            new TH3F(makeName("hdetpos").c_str(),
                     "Start positions of detected photons from one PMT; [mm]; Number of photons",
                     60, -300, 300,
                    100, -100, 900,
                     60, -300, 300);

    hbeamlambdadet =
            new TH1F(makeName("hbeamlambdadet").c_str(),
                     "Detected wavelengths; #lambda [nm]; Number of beam photons",
                      700, 200, 900);

    hbeamsegthdet  = new TH1F(makeName("hbeamsegthdet").c_str(),
                          "Start angles of detected beam photons; #Theta' [#circ]; Number of beam photons",
                          180, 0, 180);
    hbeamsegz0det  = new TH1F(makeName("hbeamsegz0det").c_str(),
                          "z-coordinates of detected beam photons; [m]; Number of beam photons",
                          100, -0.5, 0.5);
    hbeamsegstartxyd = new TH2F(makeName("hbeamsegstartxyd").c_str(),  // makeName("hsegxy0det"),
                            "xy-coordinates of detected beam photons; [mm]; [mm]",
                            300, -0.5, 1.5, 300, -0.50, 1.50);
    hbeamsegstartxzd  = new TH2F(makeName("hbeamsegstartxzd").c_str(),
                             "xz-coordinates of detected beam photons; [mm]; [mm]",
                             20, -10, 10, 600, -300, 300);
    hbeamsegxzd  = new TH2F(makeName("hbeamsegxzd").c_str(),
                        "xz-coordinates on PMT of detected beam photons; [mm]; [mm]",
                        100, -30, 30, 500, -250, 250);

    hbackgroundsegthdet  = new TH1F(makeName("hbackgroundsegthdet").c_str(),
                          "Start angles of detected background photons; #Theta' [#circ]; Number of background photons",
                          180, 0, 180);
    hbackgroundsegz0det  = new TH1F(makeName("hbackgroundsegz0det").c_str(),
                          "z-coordinates of detected background photons; [m]; Number of background photons",
                          100, -0.5, 0.5);
    hbackgroundsegstartxyd = new TH2F(makeName("hbackgroundsegstartxyd").c_str(),  // makeName("hsegxy0det"),
                            "xy-coordinates of detected background photons; [mm]; Number of background photons",
                            300, -0.5, 1.5, 300, -0.50, 1.50);
    hbackgroundsegstartxzd  = new TH2F(makeName("hbackgroundsegstartxzd").c_str(),
                             "xz-coordinates of detected background photons; [mm]; Number of background photons",
                             20, -10, 10, 600, -300, 300);
    hbackgroundsegxzd  = new TH2F(makeName("hbackgroundsegxzd").c_str(),
                        "xz-coordinates on PMT of detected background photons; [mm]; Number of background photons",
                        100, -30, 30, 500, -250, 250);


    return;
}


PMTHisto::~PMTHisto() {
    delete hdetpos;

    delete hbeamlambdadet;

    delete hbeamsegthdet;
    delete hbeamsegz0det;
    delete hbeamsegstartxyd;
    delete hbeamsegstartxzd;
    delete hbeamsegxzd;

    delete hbackgroundsegthdet;
    delete hbackgroundsegz0det;
    delete hbackgroundsegstartxyd;
    delete hbackgroundsegstartxzd;
    delete hbackgroundsegxzd;

    return;
}

void PMTHisto::Write() {
    hdetpos->Write();

    hbeamlambdadet->Write();

    hbeamsegthdet->Write();
    hbeamsegz0det->Write();

    hbeamsegstartxyd->Write();
    hbeamsegstartxzd->Write();
    hbeamsegxzd->Write();

    hbackgroundsegthdet->Write();
    hbackgroundsegz0det->Write();

    hbackgroundsegstartxyd->Write();
    hbackgroundsegstartxzd->Write();
    hbackgroundsegxzd->Write();
}

void PMTHisto::Clear() {
    hdetpos->Reset();

    hbeamlambdadet->Reset();

    hbeamsegthdet->Reset();
    hbeamsegz0det->Reset();

    hbeamsegstartxyd->Reset();
    hbeamsegstartxzd->Reset();
    hbeamsegxzd->Reset();

    hbackgroundsegthdet->Reset();
    hbackgroundsegz0det->Reset();

    hbackgroundsegstartxyd->Reset();
    hbackgroundsegstartxzd->Reset();
    hbackgroundsegxzd->Reset();
}

std::string PMTHisto::makeName(const char *name) {
    return std::string(name)+std::to_string(id);
}

}  // namespace lsr

