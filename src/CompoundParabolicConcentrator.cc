/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include <stdexcept>


#include "lsr/components/CompoundParabolicConcentrator.hh"

#include "Constants.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"

namespace lsr {
namespace components {

CompoundParabolicConcentrator::CompoundParabolicConcentrator() {
    // should be dependable on PMT active surface
    this->rExit  = 23*mm;    // radius of viewport windows
    this->rEntry = gpar->vwp_ifwrad;
    if (rExit > rEntry) throw std::invalid_argument("exit radius greater entry radius");
    this->acceptanceAngle = asin(rExit/rEntry);
}

G4Polycone* CompoundParabolicConcentrator::getG4Polycone(
        const G4String& pName) {
    // Initialize Arrays
    // Number of points is one above#
    const int steps = gpar->CPC_areas;

    double *r_inner = new double[steps+1];
    double *r_outer = new double[steps+1];
    double *z = new double[steps+1];
    const double thickness = 5*mm;

    // Get the Height
    // double height = getHeight();

    // Just used as shortener
    double phi_max = this->acceptanceAngle;
    double f = this->getF();

    // Calculate the radius
    for (int i = 0; i <= steps; ++i) {
        // double y = i* height/(steps);
        // double x = sqrt(4*this->rExit*(y+rExit)) - this->rExit;
        double phi = 2*phi_max + i*(90*deg-phi_max)/steps;

        // see formula of CPC
        double y = (2*f*cos(phi - phi_max))/(1-cos(phi));
        double x = (2*f*sin(phi - phi_max))/(1-cos(phi)) - this->rExit;

        z[i] = y;
        r_inner[i] = x;
        r_outer[i] = x + thickness;
    }


    // return Polycone
    return new G4Polycone(pName,
                          0*deg,
                          360*deg,
                          steps+1,
                          z,
                          r_inner,
                          r_outer);
}

double CompoundParabolicConcentrator::getBottomHeight() const {
    double f = this->getF();
    // phi max = maximaler Einfallswinkel
    double phi_max = this->acceptanceAngle;
    double phi_top = phi_max + 90*deg;  // Angle at the top (exit)
    double phi_bottom = 2*phi_max;  // Angle at the bottom (entry)
    double z_top = (2*f*cos(phi_top - phi_max))/(1-cos(phi_top));
    double z_bottom = (2*f*cos(phi_bottom - phi_max))/(1-cos(phi_bottom));
    return - z_top + z_bottom;
}

double CompoundParabolicConcentrator::getLength() const {
    return (rExit + rEntry)/tan(acceptanceAngle);
}

double CompoundParabolicConcentrator::getEntryRadius() const {
    double phi_max = this->acceptanceAngle;
    double phi = 2*phi_max;  // (90*deg-phi_max);

    return (2*this->getF()*sin(phi - phi_max))/(1-cos(phi)) - this->rExit;
}

G4double CompoundParabolicConcentrator::getF() const {
    return this->rExit*(1+sin(this->acceptanceAngle));
}

}  // namespace components
}  // namespace lsr

