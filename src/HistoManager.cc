/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include <stdio.h>

#include "lsr/HistoManager.hh"
#include "G4UnitsTable.hh"

namespace lsr {


HistoManager::HistoManager():fRootFile(0) {
    // histograms
    for (G4int k = 0; k < MAXHISTO; k++) {
        fHisto[k] = 0;
    }
}

HistoManager::~HistoManager() {
    if ( fRootFile ) delete fRootFile;
}

void HistoManager::book() {
    G4String fileName = "lsr_histo.root";
    fRootFile = new TFile(fileName, "RECREATE");
    if (!fRootFile) {
        G4cout << " HistoManager::book :"
               << " problem creating the ROOT TFile "
               << G4endl;
        return;
    }


    // initialize histograms
    halldetpos =
            new TH3F("halldetpos",
                     "Start positions of all detected photons",
                     60, -300, 300,
                    100, -100, 900,
                     60, -300, 300);
    // histos for beam photons
    hbeamtheta  =
            new TH1F("hbeamtheta",
                     "Polar angle; #Theta' [#circ]; Number of beam photons",
                     180, 0, 180);
    hbeamlambdacms =
            new TH1F("hbeamlamcms",
                     "Wavelength; #lambda [nm]; Number of beam photons",
                     700, 200, 900);
    hbeamlambda =
            new TH1F("hbeamlamlab",
                     "Wavelength; #lambda [nm]; Number of beam photons",
                     700, 200, 900);
    hbeamphi =
            new TH1F("hbeamphilab",
                     "Azimuth angle; #phi [#circ]; Number of beam photons",
                     360, 0, 360);
    hbeamphidet =
            new TH1F("hbeamphilabdet",
                     "Azimuth angle; #phi [#circ]; Number of beam photons",
                     360, 0, 360);
    hbeamthetacathode  = new TH1F("hbeamthetacathode",
                                  "Angles on photocathode of beam photons; #Theta [#circ]; Number of beam photons", 180, 0, 180);

    // histos for beam photons
    hbeamthetadet  =
            new TH1F("hbeamthetadet",
                     "Start angles of detected beam photons; #Theta' [#circ]; Number of beam photons",
                     180, 0, 180);

    hbeamlambdadet =
            new TH1F("hbeamlambdadet",
                     "Detected wavelengths; #lambda [nm]; Number of beam photons",
                      700, 200, 900);

    hbeamzdet  = new TH1F("hbeamzdet",
                          "z-coordinates of detected beam photons; [m]; Number of beam photons",
                          100, -0.5, 0.5);

    // histos for background photos
    hbackgroundtheta  =
            new TH1F("hbackgroundtheta",
                     "Polar angle; #Theta' [#circ]; Number of background photons",
                     180, 0, 180);
    hbackgroundthetadet  =
            new TH1F("hbackgroundthetadet",
                     "Start angles of detected background photons; #Theta' [#circ]; Number of beam photons",
                     180, 0, 180);
    hbackgroundlambda =
            new TH1F("hbackgroundlam",
                     "Wavelength; #lambda [nm]; Number of background photons",
                     700, 200, 900);
    hbackgroundphi =
            new TH1F("hbackgroundphi",
                     "Azimuth angle; #phi [#circ]; Number of background photons",
                     360, 0, 360);
    hbackgroundphidet =
            new TH1F("hbackgroundphidet",
                     "Azimuth angle; #phi [#circ]; Number of background photons",
                     360, 0, 360);
    hbackgroundthdet  = new TH1F("hbackgroundthdet",
                                 "Angles on photocathode of background photons", 180, 0, 180);

    hbackgroundlambdadet =
            new TH1F("hbackgroundlamlabdet",
                     "Wavelength; #lambda [nm]; Number of background photons",
                     700, 200, 900);
    hbackgroundzdet  = new TH1F("hbackgroundzdet",
                          "z-coordinates of detected background photons; [m]; Number of background photons",
                          100, -0.5, 0.5);



    for (G4int k = 0; k < MAXHISTO; k++) {
        char pmtname[6];
        snprintf(pmtname, sizeof(pmtname), "PMT%d", k);
        fHisto[k] = new PMTHisto(k);
    }

    G4cout << "Using histogram file:" << fileName << G4endl;
}


void HistoManager::save() {
    if (fRootFile) {
        fRootFile->Write();       // Writing the histograms to the file
        fRootFile->Close();       // and closing the tree (and the file)
        G4cout << "-- Histogram Tree is saved " << G4endl;
    }
}

PMTHisto* HistoManager::getPMTHisto(G4int id) {
    if (id >= MAXHISTO) return nullptr;
    if  (fHisto[id]) { return fHisto[id]; }
    return nullptr;
}

void HistoManager::PrintStatistic() {  // should be implemented
}

}  // namespace lsr
