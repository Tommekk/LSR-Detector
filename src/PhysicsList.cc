/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "globals.hh"
#include "lsr/PhysicsList.hh"

#include "G4SystemOfUnits.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"

namespace lsr {

PhysicsList::PhysicsList(): G4VUserPhysicsList() {
    defaultCutValue = 1.0*mm;
    SetVerboseLevel(0);
}

PhysicsList::~PhysicsList() {}


void PhysicsList::ConstructParticle() {
    // In this method, static member functions should be called
    // for all particles which you want to use.
    // This ensures that objects of these particle types will be
    // created in the program.
    G4OpticalPhoton::OpticalPhotonDefinition();
}


void PhysicsList::ConstructProcess() {
    // Define transportation process
    AddTransportation();

    // Get the process manager
    G4ParticleDefinition* particle = G4OpticalPhoton::OpticalPhoton();
    G4ProcessManager *pmanager = particle->GetProcessManager();

    // Construct processes for optical photon
    theAbsorptionProcess  = new G4OpAbsorption();
    theRayleighScattering = new G4OpRayleigh();
    theBoundaryProcess    = new G4OpBoundaryProcess();
    // theBoundaryProcess->SetModel(unified);

    // Register processes to optical photon's process manager
    pmanager->AddDiscreteProcess(theAbsorptionProcess);
    pmanager->AddDiscreteProcess(theRayleighScattering);
    pmanager->AddDiscreteProcess(theBoundaryProcess);
}


void PhysicsList::SetCuts() {
    // " G4VUserPhysicsList::SetCutsWithDefault" method sets
    // the default cut value for all particle types
    SetCutsWithDefault();

    // if (verboseLevel > 0) DumpCutValuesTable();
}

}  // namespace lsr

