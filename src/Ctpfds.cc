/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/components/Ctpfds.hh"
#include "lsr/DetectorConstruction.hh"
#include "lsr/components/IFViewportWindow.hh"

#include "Constants.hh"

#include "G4ThreeVector.hh"
#include "G4EllipticalTube.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4IntersectionSolid.hh"


namespace lsr {
namespace components {

/**
 * @brief Ctpfds::Ctpfds
 */
Ctpfds::Ctpfds() {
    ellipse_height = gpar->mirror_section_height*mm;
    ellipse_width = std::sqrt(2 * gpar->beamhole_radius * ellipse_height
                              - gpar->beamhole_radius*gpar->beamhole_radius);  // should be moved to function
    ellipse_eccentricity = std::sqrt(ellipse_height*ellipse_height-ellipse_width*ellipse_width);

    width = ellipse_width*ellipse_width/ellipse_height;

    height = width;
}

Ctpfds::~Ctpfds() {}

/**
 * @brief Ctpfds::getHeight
 * @return half height in mm
 */
G4double Ctpfds::getHeight() const {
    return height;
}

/**
 * @brief Ctpfds::getWidth
 * @return half width in mm
 */
G4double Ctpfds::getWidth() const {
    return width;
}

/**
 * @brief Ctpfds::getEccentricity
 * @return eccentricity of the ellipse in mm
 */
G4double Ctpfds::getEccentricity() const {
    // linear eccentricity
    // distance between one focal point and the middle of the ellipse
    return std::sqrt(height*height
                     - width*width);
}

G4ThreeVector Ctpfds::getPosition() const {
    return G4ThreeVector(0, 0, 0);
}

bool Ctpfds::isPointWithin(G4double x, G4double y) const {
    if (x > 0) {  // it is in the circle in the upper half
        return (x*x + y*y <= width*width);
    }  // it is in the circle at the bottom
    return (x*x/(width*width) + y*y/(height*height) <= 1.) ? true : false;
}

G4VSolid* Ctpfds::getSolid() {
    // CONDENSING-TYPE PORTABLE
    // FLUORESCENCE DETECTION SYSTEM

    G4EllipticalTube* ctpfds_ellipse =
            new G4EllipticalTube("ctpfds_ellipse",
                                 ellipse_width,
                                 ellipse_height,
                                 25*cm - 5*mm);  // should be chamber->getLength
    G4Tubs* ctpfds_circle =
            new G4Tubs("ctpfds_circle",
                       0,
                       width,
                       25*cm - 5*mm,
                       0*deg,
                       360*deg);
    G4IntersectionSolid* ctpfds =
            new G4IntersectionSolid("ctpfds",
                                    ctpfds_circle,
                                    ctpfds_ellipse,
                                    G4TranslateY3D(+ellipse_eccentricity));
    return ctpfds;
}

G4double Ctpfds::getCathodeHeight() const {
    // Calculates the Height, where to put the Viewport Window
    // with sqrt it is calculated at which height of the ellipse it has the radius
    // of the viewport window radius
    ViewportWindow* viewport = DetectorConstruction::getInstance().getIFViewportWindow();

    return height + viewport->getWindowThickness()/2;
}

}  // namespace components
}  // namespace lsr

