/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/RunAction.hh"
#include "lsr/Run.hh"

#include "G4RegionStore.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"
#include "G4ios.hh"
#include "G4UnitsTable.hh"

namespace lsr {

RunAction::RunAction(HistoManager* hstMngr) {
    histoManager = hstMngr;
}

RunAction::~RunAction() {;}


G4Run* RunAction::GenerateRun() {
    return new Run;
}


void RunAction::BeginOfRunAction(const G4Run* aRun) {
    G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
    histoManager->book();
    histoManager->N_tot = 0;
    histoManager->N_det = 0;
    histoManager->N_req = aRun->GetNumberOfEventToBeProcessed();

    G4cout << "Start of run " << aRun->GetRunID() << "\n"
           << "   Number of events requested "
           << histoManager->N_req << " "
           << "   Number of events processed "
           << aRun->GetNumberOfEvent() << "\n"
           << G4endl;
}


void RunAction::EndOfRunAction(const G4Run* aRun) {
    const Run* theRun = (const Run*) aRun;

    G4double nEvt = (G4double)(theRun->GetNumberOfEvent());

    G4cout
            << "###########################################################"
            << G4endl;
    G4cout
            << " Run Summary - Number of events : "
            << nEvt
            << G4endl;

    // detection_percentage includes photons which are not from inside the chamber!!!
    G4double detection_percentage = histoManager->N_det / histoManager->N_tot * 100;
    // equivalent observation length
    G4double chamber_percentage = histoManager->N_beam_chamber_det / histoManager->N_beam_chamber_tot * 100;
    G4double e_EOL = chamber_percentage/100 * 0.5;  // 0.5 = detector length in meter

    printf("==> N_tot = %f,  N_det = %f\n",
           histoManager->N_tot, histoManager->N_det);
    printf("==> Detection Percentage = %2.3f %%,\n==> Chamber Percentage = %2.3f%%,\n    equivalent observation length = %.5f m\n",
           detection_percentage, chamber_percentage, e_EOL);
    printf("Detected beam photons: %.0f/%.0f (%2.3f%%)\n",
           histoManager->N_beam_det, histoManager->N_beam_tot, histoManager->N_beam_det/histoManager->N_beam_tot*100);
    printf("Detected background photons: %.0f/%.0f (%2.3f%%)\n",
           histoManager->N_background_det, histoManager->N_background_tot, histoManager->N_background_det/histoManager->N_background_tot*100);
    printf("Background-to-beam-ratio: 1-%.3f, (1-1 at start)\n",
           histoManager->N_beam_det/histoManager->N_background_det);

    G4cout
            << "###########################################################"
            << G4endl;

    // save histograms to root file
    histoManager->save();
    histoManager->N_req = 0;
}

}  // namespace lsr

