/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/components/Circle.hh"


#include "Constants.hh"

#include "G4ThreeVector.hh"
#include "G4Tubs.hh"


namespace lsr {
namespace components {

/**
 * @brief Circle::Circle
 */
Circle::Circle() {
    height = gpar->mirror_section_height*mm;
    width = height;
}

Circle::~Circle() {}

/**
 * @brief Circle::getHeight
 * @return half height in mm
 */
G4double Circle::getHeight() const {
    return height;
}

/**
 * @brief Circle::getWidth
 * @return half width in mm
 */
G4double Circle::getWidth() const {
    return width;
}

/**
 * @brief Circle::getEccentricity
 * @return eccentricity of the ellipse in mm
 */
G4double Circle::getEccentricity() const {
    // linear eccentricity
    // distance between one focal point and the middle of the ellipse
    return 0;
}

G4ThreeVector Circle::getPosition() const {
    return G4ThreeVector(0, 0, 0);
}

bool Circle::isPointWithin(G4double x, G4double y) const {
    return (x*x/(width*width) + y*y/(height*height) <= 1.) ? true : false;
}

G4VSolid* Circle::getSolid() {
    return new G4Tubs("focus_tube",
                        0,
                         getWidth(),
                         25*cm - 5*mm,
                      0*deg,
                      360*deg);  // chamber->getLength() - 5*mm
}

G4double Circle::getCathodeHeight() const {
    // Calculates the Height, where to put the Viewport Window
    // with sqrt it is calculated at which height of the ellipse it has the radius
    // of the viewport window radius

    // should be dependable on setup, but getCathodeHeight can be removed, because it is not used

    return height;
}

}  // namespace components
}  // namespace lsr

