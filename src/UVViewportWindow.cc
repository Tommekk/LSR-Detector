/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/components/UVViewportWindow.hh"

#include "Constants.hh"
#include "G4Tubs.hh"


namespace lsr {
namespace components {

/**
 * @brief uvviewportwindow::uvviewportwindow
 */
UVViewportWindow::UVViewportWindow() {
    //----------------------
    // uvviewportwindow window (at the top)
    //----------------------
    window_radius     = gpar->vwp_uvwrad;        // radius of uvviewportwindow windows
    window_thickness  = gpar->vwp_uvwthk;        // thickness of uvviewportwindow windows
    height            = gpar->vwp_uvheight;      // height of Viewport
    flange_radius     = gpar->vwp_uvwflangerad;  // radius of uvviewportwindow flange
    flange_thickness  = gpar->vwp_uvwflangethk;  // thickness of uvviewportwindow flange
}

UVViewportWindow::~UVViewportWindow() {}

G4double UVViewportWindow::getWindowRadius() const {
    return window_radius;
}

G4double UVViewportWindow::getWindowThickness() const {
    return window_thickness;
}

G4double UVViewportWindow::getHeight() const {
    return height;
}

G4double UVViewportWindow::getFlangeRadius() const {
    return flange_radius;
}

G4double UVViewportWindow::getFlangeThickness() const {
    return flange_thickness;
}

G4VSolid *UVViewportWindow::getSolid() {
    return new G4Tubs("uvviewport_window",
                              0.0,                            // inner radius
                              this->getWindowRadius(),    // outer radius
                              this->getWindowThickness()/2,  // half height along z-axis
                              0.*deg,      // start angle
                              360.*deg);   // spanning angle
}

}  // namespace components
}  // namespace lsr

