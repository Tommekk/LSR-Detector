/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include <stdexcept>

#include "lsr/PrimaryGeneratorAction.hh"
#include "lsr/DetectorConstruction.hh"
#include "Constants.hh"
#include "myutils.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"

namespace lsr {

PrimaryGeneratorAction::PrimaryGeneratorAction(HistoManager* hstMngr) {
    histoManager = hstMngr;

    particleGun = new G4ParticleGun(1);
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();

    particleGun->
            SetParticleDefinition(particleTable->FindParticle("opticalphoton"));

    // initialize random number generator
    dice = new TRandom3(std::time(nullptr));  // standard was 10


    // calculate some experimental quantities
    gamma   = 1/sqrt(1-tpar->beta*tpar->beta);
    tau_lab = gamma * tpar->tau;

    // histo->N_emit = tpar->N_exc / tau_lab * gpar->s_beam/gpar->s_esr;
    // should be usable when tpar->N_exc contains right data
    histoManager->N_emit = tpar->N_exc * 50 * (1 - exp(-0.020/tau_lab))
            * gpar->s_beam/gpar->s_lsr;
    printf("==> tau_lab = %f ms, N_emit = %f 1/s\n",
           tau_lab*1e3, histoManager->N_emit);
}


PrimaryGeneratorAction::~PrimaryGeneratorAction() {
    // delete particleGun;
}


void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
    if (tpar->backgr_type < 4 && histoManager->N_tot >= histoManager->N_req/2) {
        if (tpar->backgr_type == 3)  // gas background
            GenerateGasBackgroundParticle(anEvent);
        else
            GenerateLaserBackgroundParticle(anEvent);
    } else {
        GenerateBeamParticle(anEvent);
    }
}

void PrimaryGeneratorAction::GenerateBeamParticle(G4Event* anEvent) {
    double photon_energy;  // energy of the photon [eV]
    double theta_cms;          // polar emission angle [rad]
    double phi;            // azimuthal emission angle [rad]
    double d;              // distance emission point - detector;
    double x, y;           // starting point x-y coordinates

    // generate events
    histoManager->N_tot++;
    histoManager->N_beam_tot++;

    // polar emission angle (isotropic)
    while (1) {
        theta_cms = dice->Uniform(0, M_PI);
        double tmp     = dice->Uniform(0, 1);

        if (tmp < sin(theta_cms))  // this is very important!
            break;
    }


    // store variables
    histoManager->theta  = CMSAngleToLabAngle(theta_cms, tpar->beta);
    double lambda_cms = tpar->lambda;
    // if zero create random lambdas
    if (tpar->lambda < 0.1 && tpar->lambda > -0.1) {
        double wavelength[6] = {280, 313, 393, 397, 854, 866};
        lambda_cms = wavelength[dice->Integer(6)];
    } else if (tpar->lambda < -0.9 && tpar->lambda > -1.1) {
        lambda_cms = dice->Uniform(200, 900);
    }
    histoManager->hbeamlambdacms->Fill(lambda_cms);
    histoManager->lambda =
            lambda_cms * gamma * (1 - tpar->beta*cos(histoManager->theta));

    histoManager->hbeamtheta->Fill(histoManager->theta*180/M_PI, 1);

    // wavelength
    histoManager->hbeamlambda->Fill(histoManager->lambda, 1);

    // set photon energy
    photon_energy = h_eV * clight / (histoManager->lambda*1e-9);
    particleGun->SetParticleEnergy(photon_energy*eV);

    if (tpar->beam_dist == 0) {
        x = tpar->beam_x*mm;
        y = tpar->beam_y*mm;
    } else if (tpar->beam_dist == 1) {
        x = dice->Gaus(tpar->beam_x*mm, tpar->beam_width);
        y = dice->Gaus(tpar->beam_y*mm, tpar->beam_width);
    } else {
        throw std::invalid_argument("Invalid number for beam_dist in geometry.par");
    }

    d = dice->Uniform(tpar->beam_z1, tpar->beam_z2)*m;  // 150.0*mm : Detector length

    if (d >= -25*cm && d <= 25*cm) {
        histoManager->N_beam_chamber_tot++;
    }
    // set photon starting position
    histoManager->r0.set(x, y, d);
    particleGun->SetParticlePosition(histoManager->r0);
    histoManager->isBackgroundParticle = false;

    // dice azimuthal angle
    phi = dice->Uniform(0, 2*M_PI);
    histoManager->hbackgroundphi->Fill(phi*180/M_PI, 1);
    histoManager->azimuth = phi;

    // set photon momentum direction
    G4ThreeVector v;
    v.setRThetaPhi(1.0, theta_cms, phi);
    particleGun->SetParticleMomentumDirection(v);
    particleGun->SetParticlePolarization(G4ThreeVector(0, 0, 1));

    particleGun->GeneratePrimaryVertex(anEvent);
}

void PrimaryGeneratorAction::GenerateGasBackgroundParticle(G4Event* anEvent) {
    double photon_energy;  // energy of the photon [eV]
    double theta;          // polar emission angle [rad]
    double phi;            // azimuthal emission angle [rad]
    double z;              // distance from center along z-axis
    double x, y;           // starting point x-y coordinates

    // generate events
    histoManager->N_tot++;
    histoManager->N_background_tot++;

    // polar emission angle (isotropic)
    while (1) {
        theta = dice->Uniform(0, M_PI);
        double tmp     = dice->Uniform(0, 1);

        if (tmp < sin(theta))  // this is very important!
            break;
    }
    histoManager->hbackgroundtheta->Fill(theta*180/M_PI, 1);


    // store variables
    histoManager->theta  = theta;
    histoManager->lambda = dice->Uniform(200, 900);

    // wavelength
    histoManager->hbackgroundlambda->Fill(histoManager->lambda, 1);

    // set photon energy
    photon_energy = h_eV * clight / (histoManager->lambda*1e-9);
    particleGun->SetParticleEnergy(photon_energy*eV);

    lsr::components::MirrorSection* mirror_section = lsr::DetectorConstruction::getInstance().getMirrorSection();

    do {
        x = dice->Uniform(-125*mm, 125*mm);
        y = dice->Uniform(-125*mm, 125*mm);
    } while (!mirror_section->isPointWithin(x-tpar->beam_x, y-tpar->beam_y));

    z = dice->Uniform(tpar->beam_z1, tpar->beam_z2)*m;  // 150.0*mm : Detector length

    // set photon starting position
    histoManager->r0.set(x, y, z);
    particleGun->SetParticlePosition(histoManager->r0);
    histoManager->isBackgroundParticle = true;

    // dice azimuthal angle
    phi = dice->Uniform(0, 2*M_PI);
    histoManager->hbeamphi->Fill(phi*180/M_PI, 1);
    histoManager->azimuth = phi;

    // set photon momentum direction
    G4ThreeVector v;
    v.setRThetaPhi(1.0, theta, phi);
    particleGun->SetParticleMomentumDirection(v);
    particleGun->SetParticlePolarization(G4ThreeVector(0, 0, 1));


    particleGun->GeneratePrimaryVertex(anEvent);
}

void PrimaryGeneratorAction::GenerateLaserBackgroundParticle(G4Event *anEvent) {
    double photon_energy;  // energy of the photon [eV]
    double theta;          // polar emission angle [rad]
    double phi;            // azimuthal emission angle [rad]
    double z;              // distance from center along z-axis
    double x, y;           // starting point x-y coordinates

    // generate events
    histoManager->N_tot++;
    histoManager->N_background_tot++;

    double leftlaserdistance = 1.5*m;
    double rightlaserdistance = 3.5*m;

    bool leftlaser = true;  // for tpar->backgr->type == 1

    if (tpar->backgr_type == 2)  // only right laser
        leftlaser = false;
    else if (tpar->backgr_type == 0)  // both laser
        leftlaser = dice->Rndm() < 0.5 ? true : false;

    double theta_max, theta_min;

    if (leftlaser) {
        theta_max = atan((gpar->beamhole_radius+0*mm)/leftlaserdistance);
        theta_min = atan((gpar->beamhole_radius-0*mm)/(leftlaserdistance+gpar->detector_length*2));
    } else {
        theta_max = M_PI - atan((gpar->beamhole_radius+0*mm)/rightlaserdistance);
        theta_min = M_PI - atan((gpar->beamhole_radius-0*mm)/(rightlaserdistance+gpar->detector_length*2));
    }

    phi = dice->Uniform(0, 2*M_PI);

    theta = dice->Uniform(theta_min, theta_max);  //= acos(1-2*dice->Uniform(rdm_min, rdm_max)); // dadurch nicht mehr isotrop! theta = arccos(1 - 2×RNDM)


    // polar emission angle (isotropic)
    /*
    while (1) {
        theta = dice->Uniform(0, M_PI);
        double tmp     = dice->Uniform(0, 1);

        if (tmp < sin(theta) && theta < phi_max && theta > phi_min) { // this is very important!
            break;
        }
    } */
    histoManager->hbackgroundtheta->Fill(theta*180/M_PI, 1);


    // store variables
    histoManager->theta  = theta;
    histoManager->lambda = dice->Uniform(200, 900);

    // wavelength
    histoManager->hbackgroundlambda->Fill(histoManager->lambda, 1);

    // set photon energy
    photon_energy = h_eV * clight / (histoManager->lambda*1e-9);
    particleGun->SetParticleEnergy(photon_energy*eV);

    x = y = 0;
    z = leftlaser ? -leftlaserdistance : rightlaserdistance;  // 150.0*mm : Detector length

    // set photon starting position
    histoManager->r0.set(x, y, z);
    particleGun->SetParticlePosition(histoManager->r0);
    histoManager->isBackgroundParticle = true;

    // dice azimuthal angle
    histoManager->hbeamphi->Fill(phi*180/M_PI, 1);
    histoManager->azimuth = phi;

    // set photon momentum direction
    G4ThreeVector v;
    v.setRThetaPhi(1.0, theta, phi);
    particleGun->SetParticleMomentumDirection(v);
    particleGun->SetParticlePolarization(G4ThreeVector(0, 0, 1));


    particleGun->GeneratePrimaryVertex(anEvent);
}

G4double PrimaryGeneratorAction::CMSAngleToLabAngle(double theta, double beta) {
    return acos((cos(theta)+beta)/(1+beta*cos(theta)));
}

}  // namespace lsr

