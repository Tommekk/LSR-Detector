/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include <stdexcept>


#include "lsr/components/Aperture.hh"

#include "Constants.hh"

#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Box.hh"
#include "G4SubtractionSolid.hh"
#include "G4VSolid.hh"

namespace lsr {
namespace components {

Aperture::Aperture() {}

G4VSolid *Aperture::getSolid(const G4String& pName) {
/* // should be implemented

    G4Tubs* plate = new G4Tubs("plate",
                               0,
                               gpar->vwp_ifwrad,
                               1*mm,
                               0*deg,
                               360*deg);
    G4Box* hole = new G4Box("hole", gpar->aperture_length, 2*mm, gpar->vwp_ifwrad);

    G4SubtractionSolid* aperture = new G4SubtractionSolid(pName, plate, hole);
    return aperture;*/
    return nullptr;
}


}  // namespace components
}  // namespace lsr

