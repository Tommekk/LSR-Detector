/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/components/FocusEllipse.hh"
#include "lsr/components/IFViewportWindow.hh"
#include "lsr/DetectorConstruction.hh"


#include "Constants.hh"

#include "G4ThreeVector.hh"
#include "G4EllipticalTube.hh"


namespace lsr {
namespace components {

/**
 * @brief FocusEllipse::FocusEllipse
 */
FocusEllipse::FocusEllipse() {
    height = gpar->mirror_section_height*mm;
    width = std::sqrt(2 * gpar->beamhole_radius * height
                      - gpar->beamhole_radius*gpar->beamhole_radius);
}

FocusEllipse::~FocusEllipse() {}

/**
 * @brief FocusEllipse::getHeight
 * @return half height in mm
 */
G4double FocusEllipse::getHeight() const {
    return height;
}

/**
 * @brief FocusEllipse::getWidth
 * @return half width in mm
 */
G4double FocusEllipse::getWidth() const {
    return width;
}

/**
 * @brief FocusEllipse::getEccentricity
 * @return eccentricity of the ellipse in mm
 */
G4double FocusEllipse::getEccentricity() const {
    // linear eccentricity
    // distance between one focal point and the middle of the ellipse
    return std::sqrt(height*height
                     - width*width);
}

G4ThreeVector FocusEllipse::getPosition() const {
    return G4ThreeVector(0, getEccentricity(), 0);
}

bool FocusEllipse::isPointWithin(G4double x, G4double y) const {
    return (x*x/(width*width) + y*y/(height*height) <= 1.) ? true : false;
}

G4VSolid* FocusEllipse::getSolid() {
    return new G4EllipticalTube("focus_ellipse",
                         getWidth(),
                         getHeight(),
                         gpar->detector_length - 5*mm);  // should be getFocusEllipseLength or similar
}

G4double FocusEllipse::getCathodeHeight() const {
    // Calculates the Height, where to put the Viewport Window
    // with sqrt it is calculated at which height of the ellipse it has the radius
    // of the viewport window radius
    ViewportWindow* viewport = DetectorConstruction::getInstance().getUVViewportWindow();

    return getHeight()
            * std::sqrt(1-viewport->getWindowRadius()*viewport->getWindowRadius()
                        /(getWidth()*getWidth()))
            + getEccentricity()
            + viewport->getWindowThickness()/2;
}

}  // namespace components
}  // namespace lsr

