/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "Constants.hh"
#include "lsr/DetectorConstruction.hh"
#include "lsr/components/CompoundParabolicConcentrator.hh"
#include "lsr/components/MirrorSectionFactory.hh"
#include "lsr/components/MirrorSection.hh"
#include "lsr/components/Aperture.hh"

#include "G4SystemOfUnits.hh"
#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Torus.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Paraboloid.hh"
#include "G4EllipticalCone.hh"
#include "G4UnionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4EllipticalTube.hh"
#include "G4Ellipsoid.hh"
#include "G4Polyhedra.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4NistManager.hh"
#include "G4LogicalSkinSurface.hh"

#include "globals.hh"

namespace lsr {

/*********************************************************
 * Constructor and destructor
 *********************************************************/
DetectorConstruction::DetectorConstruction() {
    mirror_section = components::MirrorSectionFactory::createMirrorSection(gpar->mirror_section_type);
    chamber       = new components::Chamber(mirror_section);
    CPC           = new components::CompoundParabolicConcentrator();
    ifvwpwindow   = new components::IFViewportWindow();
    uvvwpwindow   = new components::UVViewportWindow();

    //----------------------
    // detector setups
    //---------------------
    detector_distance = chamber->getLength()*2/(detector_count);
    detector_first_z_position = chamber->getPosition().z() - chamber->getLength() + detector_distance/2;

    if (gpar->showcasemode > 0) {
        showcasemode = true;
    } else {
        showcasemode = false;
    }


    hight_lg = CPC->getLength()/2;  // lightguide height
    rad_det  = 23*mm;  // radius photocathode's for mirror section (between spmt and lightguide)
}

DetectorConstruction::~DetectorConstruction() {}

DetectorConstruction &DetectorConstruction::getInstance() {
    static DetectorConstruction instance;
    return instance;
}


/*********************************************************
 * Construct experimental setup
 *********************************************************/
G4VPhysicalVolume* DetectorConstruction::Construct() {
    // define materials and surfaces
    DefineMaterials();
    //----------------------------------
    // experimental hall (world volume)
    //----------------------------------
    // beam line along z axis
    G4Box* experimentalHall_box =
            new G4Box("expHall_box", chamber->getWidth(), 2.0*m, gpar->s_beam*m/2);
    experimentalHall_log =
            new G4LogicalVolume(experimentalHall_box, Vacuum, "expHall_log");

    // Instantiation of a set of visualization attributes
    G4VisAttributes* Hall_VisAtt = new G4VisAttributes();
    // Set the forced solid style
    Hall_VisAtt->SetForceWireframe(true);
    // Assignment of the visualization attributes to the logical volume
    experimentalHall_log->SetVisAttributes(Hall_VisAtt);

    experimentalHall_phys = new G4PVPlacement(0, G4ThreeVector(),
                                              experimentalHall_log,
                                              "expHall", 0, false, 0);

    G4Tubs* detector_chamber = new G4Tubs("detector_chamber",
                                          0.0,  // inner radius
                                          gpar->beam_rad-1*mm,   // outer radius
                                          chamber->getLength(),  // half height along z-axis
                                          0.*deg,                // start angle
                                          360.*deg);             // spanning angle

    G4Tubs* beamhole = new G4Tubs("beamhole",
                                  0.0,              // inner radius
                                  chamber->getBeamholeRadius(),  // outer radius
                                  chamber->getBeamholeLength(),  // half height along z-axis
                                  0.*deg,           // start angle
                                  360.*deg);        // spanning angle
    // Substract tube for the beam
    G4SubtractionSolid* detector_chamber_with_beamhole =
            new G4SubtractionSolid("detector_chamber_with_beamhole",
                                   detector_chamber,
                                   beamhole,
                                   nullptr,
                                   G4ThreeVector(tpar->beam_x, tpar->beam_y, 0));
    ////////////////////////////////////////////



    //////////////////////////////////////////
    detector_solid =
            new G4SubtractionSolid("detector_pre1",
                                   detector_chamber_with_beamhole,
                                   mirror_section->getSolid(),
                                   nullptr,
                                   G4ThreeVector(tpar->beam_x, tpar->beam_y, 0) + mirror_section->getPosition());

    MakeChamberHoles();  // chamber holes

    detector_log =
            new G4LogicalVolume(detector_solid, Steel, "detector_log");
    new G4LogicalSkinSurface("detector_mirror_surface", detector_log,
                             getMaterial(gpar->mirror_section_surf));
    if (!showcasemode) {
        G4VisAttributes * detector_VisAtt = new G4VisAttributes();
        detector_VisAtt->SetForceWireframe(true);
        detector_VisAtt->SetForceAuxEdgeVisible(true);
        detector_log->SetVisAttributes(detector_VisAtt);
    }
    new G4PVPlacement(nullptr,
                      chamber->getPosition(),
                      detector_log, "detector", experimentalHall_log, false, 0);
    ///////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////
    // Detector Definition

    //----------------------
    // viewport window (at the top)
    //----------------------
    // only UVWindow should be used anymore
    components::UVViewportWindow* ifviewportwindow = new components::UVViewportWindow();
    ifviewportwindow_log = new G4LogicalVolume(ifviewportwindow->getSolid(), Saphir/*Kodial*/, "ifviewport_window_log");

    // ifviewportwindow_log->SetVisAttributes(win_kodial_VisAtt);

    components::UVViewportWindow* uvviewportwindow = new components::UVViewportWindow();
    uvviewportwindow_log = new G4LogicalVolume(uvviewportwindow->getSolid(), Saphir , "uvviewport_window_log");
    G4VisAttributes* win_saphir_VisAtt = new G4VisAttributes(G4Colour(1., 1., 0.));
    uvviewportwindow_log->SetVisAttributes(win_saphir_VisAtt);

    // reentrance tubes
    G4double retub1_height = 125*mm-std::sqrt(125*125*mm*mm-uvvwpwindow->getWindowRadius()*uvvwpwindow->getWindowRadius());

    G4VSolid* retub1 = new G4Tubs("retub1",
                                  uvvwpwindow->getWindowRadius(),      // inner radius
                                  uvvwpwindow->getWindowRadius()+1.5,  // outer radius
                                  retub1_height,  // half height along z-axis
                                  0.*deg,        // start angle
                                  360.*deg);     // spanning angle
    retub1 = new G4SubtractionSolid("retub1_cutted",
                                    retub1,
                                    detector_solid,
                                    nullptr,
                                    G4ThreeVector(0, 0, -125*mm+retub1_height));  // should be dependable on chamber radius


    G4Tubs* retub2 = new G4Tubs("retub2",
                                uvvwpwindow->getWindowRadius(),  // inner radius
                                uvvwpwindow->getFlangeRadius(),      // outer radius
                                uvvwpwindow->getFlangeThickness()/2,    // thickness along z-axis
                                0.*deg,        // start angle
                                360.*deg);     // spanning angle
    G4UnionSolid* retub =
            new G4UnionSolid("retub", retub2, retub1, nullptr,
                             G4ThreeVector(0, 0, retub1_height*2));
    retub_log =  new G4LogicalVolume(retub, Steel, "retub_log");
    if (!showcasemode) {
        G4VisAttributes* retub_VisAtt = new G4VisAttributes();
        retub_VisAtt->SetForceWireframe(true);
        retub_VisAtt->SetForceAuxEdgeVisible(true);
        retub_log->SetVisAttributes(retub_VisAtt);
    }

    //-------------
    // lightguides
    //-------------
    // should be dependable on PMT size
    G4Cons* lightguide = new G4Cons("lightguide",
                                    0.0,        // inner radius at -pDz
                                    23,         // outer radius at -pDz  // should save pmt active area in variable
                                    0.0,        // inner radius at +pDz
                                    uvvwpwindow->getWindowRadius(),         // outer radius at +pDz
                                    hight_lg,   // half height along z-axis
                                    0.*deg,     // start angle
                                    360.*deg);  // spanning angle
    lightguide_log =
            new G4LogicalVolume(lightguide, PGlass, "lightguide_log");
    // Define visualization attributes with cyan colour
    G4VisAttributes* lightguide_VisAtt =
            new G4VisAttributes(G4Colour(0., 1., 1.));
    lightguide_log->SetVisAttributes(lightguide_VisAtt);

    //-------------
    // cone (alternative to lightguide or CPC)
    //-------------
    G4Cons* cone = new G4Cons("cone",
                              23*mm,        // inner radius at -pDz
                              23*mm+1*mm,         // outer radius at -pDz  // should save pmt active area in variable
                              uvvwpwindow->getWindowRadius()-1*mm,        // inner radius at +pDz
                              uvvwpwindow->getWindowRadius(),         // outer radius at +pDz
                              hight_lg,   // half height along z-axis
                              0.*deg,     // start angle
                              360.*deg);  // spanning angle
    cone_log =
            new G4LogicalVolume(cone, Steel, "cone_log");
    if (!showcasemode) {
        G4VisAttributes * cone_VisAtt = new G4VisAttributes();
        cone_VisAtt->SetForceWireframe(true);
        cone_VisAtt->SetForceAuxEdgeVisible(true);
        cone_log->SetVisAttributes(cone_VisAtt);
    }
    new G4LogicalSkinSurface("cone_mirror_surface", cone_log, getMaterial(gpar->tower_surf));


    //--------------------------
    // pmt's for mirror section (Photomultiplier)
    //--------------------------
    G4Tubs* spmt = new G4Tubs("spmt",
                              0.0*cm,     // inner radius
                              25,         // outer radius
                              90.0/2,     // height along z-axis
                              0.*deg,     // start angle
                              360.*deg);  // spanning angle
    spmt_log = new G4LogicalVolume(spmt, Glass, "spmt_log");

    // Define visualization attributes with orange colour
    G4VisAttributes * spmt_VisAtt = new G4VisAttributes(G4Colour(1., 0.65, 0.));
    // spmt_VisAtt->SetForceWireframe(true);
    // Assignment of the visualization attributes to the logical volume
    spmt_log->SetVisAttributes(spmt_VisAtt);

    //------------------------------------
    // photocathode's for mirror section (between spmt and lightguide)
    //------------------------------------
    // R1017
    G4Tubs* scath =
            new G4Tubs("scath", 0.0, rad_det, 1.0/2, 0., 360.*deg);
    scath_log  =
            new G4LogicalVolume(scath, Steel, "scath_log");
    // add photosensitive surface to volume
    new G4LogicalSkinSurface("scath_surf", scath_log, Photocath_surf);

    ///////////////////////////////////////////////////////////////////////////
    // CPC Definition (Compound Parabolic Concentrator)
    // Is intended to focus light on the photocathode

    G4Polycone* CPC_polycone = CPC->getG4Polycone("CPC_polycone");
    CPC_log = new G4LogicalVolume(CPC_polycone, Steel, "CPC_log");
    if (!showcasemode) {
        G4VisAttributes * CPC_VisAtt = new G4VisAttributes();
        CPC_VisAtt->SetForceWireframe(true);
        CPC_VisAtt->SetForceAuxEdgeVisible(true);
        CPC_log->SetVisAttributes(CPC_VisAtt);
    }
    new G4LogicalSkinSurface("CPC_mirror_surface", CPC_log, getMaterial(gpar->tower_surf));

    ////////////////////////////////////////////////////////////////////////////



    ////////////////////////////////////////////////////////////////////////////
    lsr::components::Aperture* aperture = new lsr::components::Aperture();
    G4VSolid* aperture_solid = aperture->getSolid("aperture");
    aperture_log = new G4LogicalVolume(aperture_solid, Steel, "aperture_log");
    PlaceDetectors();
    //////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////
    //----------------------------------
    // beam tube
    //----------------------------------

    // construct cylindrical tube body
    G4Tubs* beam_cyl = new G4Tubs("beam_cyl",
                                  gpar->beam_rad,      // inner radius
                                  gpar->beam_rad+5.0*mm,  // outer radius
                                  gpar->s_beam*m/2,    // half height on z-axis
                                  0.*deg,              // start angle
                                  360.*deg);           // spanning angle
    // make CF100 exits
    G4Tubs* beam_exit = new G4Tubs("beam_exit",
                                   0,               // inner radius
                                   50.0,            // outer radius
                                   10.0,            // half height along z-axis
                                   0.*deg,          // start angle
                                   360.*deg);       // spanning angle
    G4RotationMatrix* RotBE = new G4RotationMatrix();
    RotBE->rotateX(G4double(90.*deg));



    // should be dependable on PMT number
    G4SubtractionSolid* beamtube_pre =
            new G4SubtractionSolid("beamtube_pre0",
                                   beam_cyl,
                                   beam_exit,
                                   RotBE,
                                   G4ThreeVector(0, gpar->beam_rad, -detector_distance));
    beamtube_pre =
            new G4SubtractionSolid("beamtube_pre1",
                                   beamtube_pre,
                                   beam_exit,
                                   RotBE,
                                   G4ThreeVector(0, gpar->beam_rad, 0));
    beamtube_pre =
            new G4SubtractionSolid("beamtube_pre2",
                                   beamtube_pre,
                                   beam_exit,
                                   RotBE,
                                   G4ThreeVector(0, gpar->beam_rad, detector_distance));


    beamtube_log =
            new G4LogicalVolume(beamtube_pre, Steel, "beamtube_log");
    // add surface to beam pipe
    new G4LogicalSkinSurface("pipe_surface", beamtube_log, Pipe_surf);
    G4VisAttributes* Beamtube_VisAtt =
            new G4VisAttributes(G4Colour(1., 0., 0.));
    Beamtube_VisAtt->SetForceWireframe(true);
    // Beamtube_VisAtt->SetForceAuxEdgeVisible(true);
    beamtube_log->SetVisAttributes(Beamtube_VisAtt);
    beamtube_phys = new G4PVPlacement(nullptr,
                                      G4ThreeVector(0, 0, 0),
                                      beamtube_log, "beamtube",
                                      experimentalHall_log, false, 0);

    return experimentalHall_phys;
}

/*********************************************************
 * Define materials and surfaces
 *********************************************************/
void DetectorConstruction::DefineMaterials() {
    G4NistManager* man = G4NistManager::Instance();

    const G4int OPT_NUMREAL = 19;

    // Photon energies at:
    G4double Ephoton[OPT_NUMREAL] =
            // 4130nm, 1550nm, 1000nm,  950nm,   900nm,   850nm,   900nm,   750nm,   700nm,   650nm,  600 nm,   550 nm,  500 nm,  450nm,  400 nm,   350 nm,  300 nm,  250 nm,  200 nm
    {0.3*eV, 0.8*eV, 1.24*eV, 1.31*eV, 1.38*eV, 1.46*eV, 1.55*eV, 1.65*eV, 1.77*eV, 1.91*eV, 2.07*eV, 2.25*eV, 2.48*eV, 2.76*eV, 3.10*eV, 3.54*eV, 4.13*eV, 4.96*eV, 6.2*eV};


    //
    // Air
    //
    Air   = man->FindOrBuildMaterial("G4_AIR");

    G4double Air_RIND[OPT_NUMREAL] =
    {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

    G4MaterialPropertiesTable *Air_mt = new G4MaterialPropertiesTable();
    Air_mt->AddProperty("RINDEX", Ephoton, Air_RIND, OPT_NUMREAL);
    Air->SetMaterialPropertiesTable(Air_mt);

    //
    // Glass G4_Pyrex_Glass G4_GLASS_PLATE
    //
    Glass = man->FindOrBuildMaterial("G4_GLASS_PLATE");

    G4double Glass_RIND[OPT_NUMREAL]  =
    {1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46};
    G4double Glass_ABSL[OPT_NUMREAL]  =
    {4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m};

    G4MaterialPropertiesTable *Glass_mt = new G4MaterialPropertiesTable();
    Glass_mt->AddProperty("ABSLENGTH", Ephoton, Glass_ABSL, OPT_NUMREAL);
    Glass_mt->AddProperty("RINDEX",    Ephoton, Glass_RIND, OPT_NUMREAL);
    Glass->SetMaterialPropertiesTable(Glass_mt);
    // because of same base material "G4_GLASS_PLATE", Glass and PGlass have the same propertys!!!


    //
    // Filter
    //
    /*
    Filter = man->FindOrBuildMaterial("G4_GLASS_PLATE");

    G4double Filter_RIND[OPT_NUMREAL]  =
    {1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46,  1.46};
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  900,  750,  700,  650,  600,  550,  500,  450,  400,  350} [nm]
    G4double Filter_ABSL[OPT_NUMREAL]  =
            // {0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm}; //bandpassfilter rot
            // {4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m}; // kein filter
    {4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*mm, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm, 0.001*mm};  // rotfilter wie bei gsi testmessung

    G4MaterialPropertiesTable *Filter_mt = new G4MaterialPropertiesTable();
    Filter_mt->AddProperty("ABSLENGTH", Ephoton, Filter_ABSL, OPT_NUMREAL);
    Filter_mt->AddProperty("RINDEX",    Ephoton, Filter_RIND, OPT_NUMREAL);
    Filter->SetMaterialPropertiesTable(Filter_mt); */


    //
    // Plexiglass
    // G4_PLEXIGLASS
    PGlass = man->FindOrBuildMaterial("G4_GLASS_PLATE");
    G4double PGlass_RIND[OPT_NUMREAL]  =
    {1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49,  1.49, 1.49,  1.49,  1.49, 1.49};
    G4double PGlass_ABSL[OPT_NUMREAL]  =
    {4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m, 4.2*m};
    // { 0, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0,0,0,0,0,0,0};


    G4MaterialPropertiesTable *PGlass_mt = new G4MaterialPropertiesTable();
    PGlass_mt->AddProperty("ABSLENGTH", Ephoton, PGlass_ABSL, OPT_NUMREAL);
    PGlass_mt->AddProperty("RINDEX",    Ephoton, PGlass_RIND, OPT_NUMREAL);
    PGlass->SetMaterialPropertiesTable(PGlass_mt);



    //
    // Kodial
    /* Kodial = man->FindOrBuildMaterial("G4_GLASS_LEAD");
    Kodial->SetName("KODIAL");
    G4double Kodial_TransProb[OPT_NUMREAL] =
    //  {4130, 1550, 1000,  950,  900,  850,  900,  750,  700,  650,  600,  550,  500,  450,  400,  350, 300,   250, 200} [nm]
       {0.,   0.92, 0.82,  0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.92, 0.92, 0.92, 0.9,  0.7, 0.36, 0.15, 0.0 };
   // { 0, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0,0,0,0,0,0,0};

    G4double Kodial_ABSL[OPT_NUMREAL];
    for (int i = 0; i < OPT_NUMREAL; ++i) {
        Kodial_ABSL[i] = CalcTransmissionProbabilityToAbsorptionLength(Kodial_TransProb[i], gpar->vwp_ifwthk);
        printf("Kodial_ABSL[%d] = %f", i, Kodial_ABSL[i]);
    }

    G4double Kodial_RIND[OPT_NUMREAL]  =
//  {4130, 1550,   1000,   950,   900,   850,   800,   750,   700,   650,   600,   550,   500,   450,   400,  350,   300,   250, 200} [nm]
    {1.40,  1.50,  1.51,  1.51,  1.51,  1.51,  1.51,  1.51,  1.51,  1.51,  1.52,  1.52,  1.52,  1.53,  1.53, 1.54,  1.54,  1.54, 1.54};

    G4MaterialPropertiesTable *Kodial_mt = new G4MaterialPropertiesTable();
    Kodial_mt->AddProperty("ABSLENGTH", Ephoton, Kodial_ABSL, OPT_NUMREAL);
    Kodial_mt->AddProperty("RINDEX",    Ephoton, Kodial_RIND, OPT_NUMREAL);
    Kodial->SetMaterialPropertiesTable(Kodial_mt); */



    // Saphir
    Saphir = man->FindOrBuildMaterial("G4_Pyrex_Glass");
    Saphir->SetName("SAPHIR");
    G4double Saphir_TransProb[OPT_NUMREAL] =
            //  {4130, 1550, 1000,  950,  900,  850,  800,  750,  700,  650,  600,  550,  500,  450,  400,  350, 300,   250, 200} [nm]
    {0.85, 0.82, 0.82,  0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.82, 0.81, 0.7 };
    //  { 0, 0, 0, 0, 0, 0, 0, 0,0,0,0,0,0,0,0,0,0,0,0};
    G4double Saphir_ABSL[OPT_NUMREAL];
    for (int i = 0; i < OPT_NUMREAL; ++i) {
        Saphir_ABSL[i] = CalcTransmissionProbabilityToAbsorptionLength(Saphir_TransProb[i], gpar->vwp_uvwthk);
        printf("Saphir_ABSL[%d] = %f", i, Saphir_ABSL[i]);
    }

    G4double Saphir_RIND[OPT_NUMREAL]  =
            //  {4130, 1550,   1000,   950,   900,   850,   800,   750,   700,   650,   600,   550,   500,   450,   400,  350,   300,   250, 200} [nm]
    {1.66,  1.74,  1.75,  1.75,  1.75,  1.75,  1.76,  1.76,  1.76,  1.76,  1.76,  1.77,  1.76,  1.78,  1.79, 1.54,  1.81,  1.84, 1.9};

    G4MaterialPropertiesTable *Saphir_mt = new G4MaterialPropertiesTable();
    Saphir_mt->AddProperty("ABSLENGTH", Ephoton, Saphir_ABSL, OPT_NUMREAL);
    Saphir_mt->AddProperty("RINDEX",    Ephoton, Saphir_RIND, OPT_NUMREAL);
    Saphir->SetMaterialPropertiesTable(Saphir_mt);


    //
    // Stainless steel
    //
    G4Element* Fe = new G4Element("Iron",   "Fe", 26., 55.8500*g/mole);
    G4Element* C  = new G4Element("Carbon", "C",   6., 12.0110*g/mole);
    G4Element* Co = new G4Element("Cobalt", "Co", 27., 58.9332*g/mole);
    Steel = new G4Material("Steel", 7.7*g/cm3, 3);
    Steel->AddElement(C, 0.04);
    Steel->AddElement(Fe, 0.88);
    Steel->AddElement(Co, 0.08);

    //
    // Vacuum
    //
    G4double density     = 1.e-15*g/cm3;
    G4double pressure    = 1.e-15*bar;
    G4double temperature = 293.*kelvin;
    G4int ncomponents    = 1;
    G4int fractionmass   = 1;
    Vacuum = new G4Material("Vacuum",
                            density,
                            ncomponents,
                            kStateGas,
                            temperature,
                            pressure);
    Vacuum->AddMaterial(Air, fractionmass);
    G4double Vacuum_RIND[OPT_NUMREAL] =
    {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

    G4MaterialPropertiesTable *Vacuum_mt = new G4MaterialPropertiesTable();
    Vacuum_mt->AddProperty("RINDEX", Ephoton, Vacuum_RIND, OPT_NUMREAL);
    Vacuum->SetMaterialPropertiesTable(Vacuum_mt);


    //
    // Mirror surface
    //
    /*   Mirror_surf = new G4OpticalSurface("mirror_surface");
    Mirror_surf->SetType(dielectric_metal);
    Mirror_surf->SetFinish(polished);
    Mirror_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  900,  750,  700,  650,  600,  550,  500,  450,  400,  350} [nm]
    G4double Mirror_REFL[OPT_NUMREAL] =
            // {0.95, 0.90, 0.84, 0.82, 0.81, 0.79, 0.81, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93};  // (spiegel altes exp.) MIRO2
            // {0.10, 0.40, 0.65, 0.68, 0.74, 0.78, 0.82, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93};  // (spiegel altes exp.) neu
            // {0.92, 0.92, 0.92, 0.90, 0.84, 0.78, 0.82, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93};  // (spiegel altes exp.) mit minimum
    {0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.95, 0.93, 0.70, 0.60, 0.55, 0.48, 0.40};  // kupfer/gold
    // {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00};  // best
    G4MaterialPropertiesTable *Mirror_mt = new G4MaterialPropertiesTable();
    Mirror_mt->AddProperty("REFLECTIVITY", Ephoton, Mirror_REFL, OPT_NUMREAL);

    Mirror_surf->SetMaterialPropertiesTable(Mirror_mt); */

    //
    // MIRO2 surface
    //
    MIRO2_surf = new G4OpticalSurface("mirror_surface");
    MIRO2_surf->SetType(dielectric_metal);
    MIRO2_surf->SetFinish(polished);
    MIRO2_surf->SetModel(unified);
    // Wavelength:
    G4double MIRO2_REFL[OPT_NUMREAL] =
            //  {4130, 1550, 1000,  950,  900,  850,  900,  750,  700,  650,  600,  550,  500,  450,  400,  350,  300,  250, 200} [nm]
    {0.95, 0.90, 0.84, 0.82, 0.81, 0.79, 0.81, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.85, 0.90, 0.40, 0.30, 0.13};  // from diagramm daniel anielski
    // {0.95, 0.90, 0.84, 0.82, 0.81, 0.79, 0.81, 0.85, 0.88, 0.90, 0.92, 0.92, 0.93, 0.93, 0.93, 0.93, 0.40, 0.30, 0.13};  // (spiegel altes exp.) MIRO2 //
    // {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00};  // best
    G4MaterialPropertiesTable *MIRO2_mt = new G4MaterialPropertiesTable();
    MIRO2_mt->AddProperty("REFLECTIVITY", Ephoton, MIRO2_REFL, OPT_NUMREAL);

    MIRO2_surf->SetMaterialPropertiesTable(MIRO2_mt);

    //
    // 90 % reflectivity surface
    //
    R90_surf = new G4OpticalSurface("mirror_surface");
    R90_surf->SetType(dielectric_metal);
    R90_surf->SetFinish(polished);
    R90_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  800,  750,  700,  650,  600,  550,  500,  450,  400,  350, 300, 250, 200} [nm]
    G4double R90_REFL[OPT_NUMREAL] =
    {0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90, 0.90};
    G4MaterialPropertiesTable *R90_mt = new G4MaterialPropertiesTable();
    R90_mt->AddProperty("REFLECTIVITY", Ephoton, R90_REFL, OPT_NUMREAL);

    R90_surf->SetMaterialPropertiesTable(R90_mt);

    // Beam pipe surface
    //
    Pipe_surf = new G4OpticalSurface("pipe_surface");
    Pipe_surf->SetType(dielectric_metal);
    Pipe_surf->SetFinish(polished);
    Pipe_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,  900,  850,  900,  750,  700,  650,  600,  550,  500,  450,  400,  350, 300, 250, 200} [nm]
    G4double Pipe_REFL[OPT_NUMREAL] =
    {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00};    // no refectivity
    // {0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25}; // (values from measurement)
    // {0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40, 0.40}; // (values from paper)
    // {0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55, 0.55}; // (values buch hans werner)
    // {1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00}; // best
    G4MaterialPropertiesTable *Pipe_mt = new G4MaterialPropertiesTable();
    Pipe_mt->AddProperty("REFLECTIVITY", Ephoton, Pipe_REFL, OPT_NUMREAL);
    Pipe_surf->SetMaterialPropertiesTable(Pipe_mt);

    //
    // Photocathode surface properties
    //
    Photocath_surf = new G4OpticalSurface("photocath_surf");
    Photocath_surf->SetType(dielectric_metal);
    Photocath_surf->SetFinish(polished);
    Photocath_surf->SetModel(unified);
    // Wavelength:
    //  {4130, 1550, 1000,  950,    900,    850,    900,    750,    700,    650,    600,    550,    500,    450,    400,    350} [nm]
    G4double Photocath_EFF[OPT_NUMREAL] =
            // r1017  selektiert last!
            // {0.00, 0.00, 0.00, 0.00, 0.0013, 0.0170, 0.0410, 0.0750, 0.1200, 0.1400, 0.1500, 0.1500, 0.1340, 0.0900, 0.0500, 0.0250};
            // r1017selektiert!
            // {0.00, 0.00, 0.00, 0.00, 0.0000, 0.0100, 0.0400, 0.0900, 0.1300, 0.1500, 0.1600, 0.1600, 0.1400, 0.1200, 0.0700, 0.0400};
            /// {0.00, 0.00, 0.00, 0.00, 0.0040, 0.0071, 0.0232, 0.0411, 0.0648, 0.0892, 0.1071, 0.1121, 0.1023, 0.0900, 0.0350, 0.0290};  // r1017
            // {0.00, 0.00, 0.00, 0.00, 0.0056, 0.0099, 0.0325, 0.0575, 0.0972, 0.1249, 0.1499, 0.1569, 0.1432, 0.1120, 0.0490, 0.0390};  // r1017 *1.4
            // {0.00, 0.00, 0.00, 0.00, 0.0064, 0.0113, 0.0371, 0.0678, 0.1037, 0.1427, 0.1736, 0.1793, 0.1636, 0.1290, 0.0790, 0.0440};  // r1017 *1.6
    {1.00, 1.00, 1.00, 1.00, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000, 1.0000};  // best

    // {0.0, 0.0, 0.0, 0.001, 0.017, 0.101, 0.116, 0.124, 0.13, 0.139, 0.149, 0.159, 0.17, 0.18, 0.19, 0.2}; // r943-02
    // {0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.02, 0.065, 0.10, 0.138, 0.158, 0.169, 0.177, 0.185, 0.17, 0.09, 0.05}; // r8900
    // {0.0, 0.0, 0.0, 0.0, 0.001, 0.003, 0.07, 0.09, 0.13, 0.15, 0.16, 0.17, 0.18, 0.17, 0.09, 0.05}; // r7400U-20
    // 4130nm,1550nm,1000nm,950nm,900nm,850nm, 900nm, 750nm,700nm,650nm, 600nm, 550nm, 500nm, 450nm,400nm,350nm

    // {0., 0., 0., 0., 0.00, 0.00, 0.0, 0.0, 0.01, 0.029, 0.055, 0.08, 0.10, 0.105, 0.108, 0.108}; // CPM 1993
    // {0., 0., 0., 0., 0.00, 0.00, 0.0, 0.0, 0.09, 0.261, 0.495, 0.72, 0.90, 0.945, 0.972, 0.972}; // CPM 1993*9
    // {0., 0., 0., 0., 0.001, 0.006, 0.015, 0.03, 0.047, 0.07, 0.088, 0.10, 0.101, 0.10, 0.09, 0.07}; // CPM 972P
    // {0., 0., 0., 0., 0.001, 0.15, 0.18, 0.20, 0.18, 0.18, 0.16, 0.15, 0.10, 0.08, 0.03, 0.1}; // H7421-50
    // {0.0, 0., 0., 0., 0., 0.000, 0.0, 0.0, 0.0, 0.08, 0.32, 0.38, 0.42, 0.42, 0.28, 0.12, 0.11}; // H7421-40

    G4double Photocath_REFL[OPT_NUMREAL] =
    {0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.};
    G4MaterialPropertiesTable* Photocath_mt = new G4MaterialPropertiesTable();
    Photocath_mt->AddProperty("EFFICIENCY", Ephoton,
                              Photocath_EFF, OPT_NUMREAL);
    Photocath_mt->AddProperty("REFLECTIVITY", Ephoton,
                              Photocath_REFL, OPT_NUMREAL);

    Photocath_surf->SetMaterialPropertiesTable(Photocath_mt);

    // dump material properties
    G4cout << *(G4Material::GetMaterialTable());

    return;
}


void DetectorConstruction::placeUVDetector(G4double cathode_height, G4double zpos, G4int number) {
    std::string number_as_string = std::to_string(number);

    G4RotationMatrix* RotX90 = new G4RotationMatrix();
    RotX90->rotateX(G4double(-90.*deg));

    if (gpar->vwp_uvfocustype == 0) {  // use CPC
        new G4PVPlacement(RotX90,
                          G4ThreeVector(0, cathode_height+uvvwpwindow->getWindowThickness()+CPC->getBottomHeight()+1*mm, zpos),  // should be calculated by components
                          CPC_log,
                          "CPC" + number_as_string,
                          experimentalHall_log,
                          false,
                          0);
    } else if (gpar->vwp_uvfocustype == 1) {  // use lightguide
        new G4PVPlacement(RotX90,
                          G4ThreeVector(0, cathode_height+uvvwpwindow->getWindowThickness()+hight_lg+1*mm, zpos),
                          lightguide_log,
                          "lightguide" + number_as_string,
                          experimentalHall_log,
                          false,
                          0);
    } else if (gpar->vwp_uvfocustype == 2) {  // use cone
        new G4PVPlacement(RotX90,
                          G4ThreeVector(0, cathode_height+uvvwpwindow->getWindowThickness()+hight_lg+1*mm, zpos),
                          cone_log,
                          "cone" + number_as_string,
                          experimentalHall_log,
                          false,
                          0);
    }


    new G4PVPlacement(RotX90,
                      G4ThreeVector(0, cathode_height+uvvwpwindow->getWindowThickness()/2, zpos),
                      uvviewportwindow_log,
                      "win_saphir" + number_as_string,
                      experimentalHall_log,
                      false,
                      0);
    new G4PVPlacement(RotX90,
                      G4ThreeVector(0, cathode_height+uvvwpwindow->getFlangeThickness()/2, zpos),
                      retub_log,
                      "retub" + number_as_string,
                      experimentalHall_log,
                      false,
                      0);
    new G4PVPlacement(RotX90,
                      G4ThreeVector(0,
                                    cathode_height+uvvwpwindow->getWindowThickness()+2*hight_lg+90.0/2+2*mm,
                                    zpos),
                      spmt_log, "spmt" + number_as_string,
                      experimentalHall_log,
                      false, 0);
    new G4PVPlacement(RotX90,
                      G4ThreeVector(0,
                                    cathode_height+uvvwpwindow->getWindowThickness()+2*hight_lg+2*mm, // should be calculated by components
                                    zpos),
                      scath_log,
                      "scath" + number_as_string,
                      experimentalHall_log,
                      false,
                      0);
}

void DetectorConstruction::placeIFDetector(G4double cathode_height, G4double zpos, G4int number) {
    std::string number_as_string = std::to_string(number);

    G4RotationMatrix* RotX90 = new G4RotationMatrix();
    RotX90->rotateX(G4double(-90.*deg));

    if (gpar->vwp_iffocustype == 0) {  // use CPC
        new G4PVPlacement(RotX90,
                          G4ThreeVector(0, cathode_height+ifvwpwindow->getWindowThickness()+CPC->getBottomHeight()+1*mm, zpos),
                          CPC_log,
                          "CPC" + number_as_string,
                          experimentalHall_log,
                          false,
                          0);
    } else if (gpar->vwp_iffocustype == 1) {  // use lightguide
        new G4PVPlacement(RotX90,
                          G4ThreeVector(0, cathode_height+ifvwpwindow->getWindowThickness()+hight_lg+1*mm, zpos),
                          lightguide_log,
                          "lightguide" + number_as_string,
                          experimentalHall_log,
                          false,
                          0);
    }  else if (gpar->vwp_uvfocustype == 2) {  // use cone
        new G4PVPlacement(RotX90,
                          G4ThreeVector(0, cathode_height+uvvwpwindow->getWindowThickness()+hight_lg+1*mm, zpos),
                          cone_log,
                          "cone" + number_as_string,
                          experimentalHall_log,
                          false,
                          0);
    }


    new G4PVPlacement(RotX90,
                      G4ThreeVector(0, cathode_height+ifvwpwindow->getWindowThickness()/2, zpos),
                      ifviewportwindow_log,
                      "win_kodial" + number_as_string,
                      experimentalHall_log,
                      false,
                      0);
    new G4PVPlacement(RotX90,
                      G4ThreeVector(0, cathode_height+ifvwpwindow->getFlangeThickness()/2, zpos),
                      retub_log,
                      "retub" + number_as_string,
                      experimentalHall_log,
                      false,
                      0);
    new G4PVPlacement(RotX90,
                      G4ThreeVector(0,
                                    cathode_height+ifvwpwindow->getWindowThickness()+2*hight_lg+90.0/2+2*mm,
                                    zpos),
                      spmt_log, "spmt" + number_as_string,
                      experimentalHall_log,
                      false, 0);
    new G4PVPlacement(RotX90,
                      G4ThreeVector(0,
                                    cathode_height+ifvwpwindow->getWindowThickness()+2*hight_lg+2*mm,
                                    zpos),
                      scath_log,
                      "scath" + number_as_string,
                      experimentalHall_log,
                      false,
                      0);
}

double DetectorConstruction::CalcTransmissionProbabilityToAbsorptionLength(double transmissionprobability, double glasstickness) {
    double abs_length = 0;
    if (transmissionprobability == 0.) {
        abs_length = 0;
    } else {
        abs_length = - glasstickness / log(transmissionprobability);
    }

    // glassthickness in mm

    printf("Abs_length: %f, trans_prob: %f, glasthicknes: %f\n", abs_length, transmissionprobability, glasstickness);
    return abs_length;
}

G4OpticalSurface *DetectorConstruction::getMaterial(int material_number) {
    if (material_number == 2) {
        return MIRO2_surf;
    }
    if (material_number == 90) {
        return R90_surf;
    }
    throw std::invalid_argument("Invalid material_number. Material does not exist!");
}



void DetectorConstruction::PlaceDetectors() {
    // double cathode_height = CPC_position_y;

    // G4double cathode_height = mirror_section->getCathodeHeight();
    // Height is the tube radius
    G4double cathode_height = gpar->beam_rad;

    // make lightguide as long as CPC
    hight_lg = CPC->getLength()/2;




    G4double zpos = detector_first_z_position;
    for (int i = 0; i < detector_count; i++, zpos += detector_distance) {
        // UV-detector
        //  if (i == 1) {
        placeUVDetector(cathode_height, zpos, i);
        //  }  else {
        //      placeIFDetector(cathode_height, zpos, i);
        //  }


        /*  new G4PVPlacement(RotX90,
                      G4ThreeVector(0, CPC_position_y, zpos),
                      CPC_log,
                      "CPC" + i_as_string,
                      experimentalHall_log,
                      false,
                      0); */
    }
}

void DetectorConstruction::MakeChamberHoles() {
    // Make hole for CPC
    G4Tubs* detector_hole_cyl =
            new G4Tubs("detector_hole_cyl",
                       0,                      // inner radius
                       uvvwpwindow->getWindowRadius(),  // outer radius
                       gpar->beam_rad-tpar->beam_y,      // half height along z-axis
                       0.*deg,                 // start angle
                       360.*deg);              // spanning angle

    double zpos = detector_first_z_position;
    G4RotationMatrix* RotX90 = new G4RotationMatrix();
    RotX90->rotateX(G4double(-90.*deg));

    for (int i = 0; i < detector_count; i++, zpos += detector_distance) {
        detector_solid = new G4SubtractionSolid("detector_pre" + std::to_string(i),
                                                detector_solid,
                                                detector_hole_cyl,
                                                RotX90,
                                                G4ThreeVector(0, chamber->getHeight(), zpos));
    }
}

components::UVViewportWindow *DetectorConstruction::getUVViewportWindow() const {
    return uvvwpwindow;
}

components::IFViewportWindow *DetectorConstruction::getIFViewportWindow() const {
    return ifvwpwindow;
}

void DetectorConstruction::printStatistic() const {}  // should be implemented

components::MirrorSection *DetectorConstruction::getMirrorSection() const {
    return mirror_section;
}


}  // namespace lsr

