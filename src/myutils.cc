/*******************************************************
 *
 * myutils.c
 *
 * Author: Volker Hannen
 *
 *******************************************************/
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <math.h>
#include <sys/types.h>

#include "myutils.hh"

#define MAXSTRINGLENGTH 200


/************************************************************************
 *
 * Collection of utility routines to read configuration files, check
 * for keyboard input, etc. .
 *
 ************************************************************************/

int get_string(FILE * file, char *text) {
    int l;

    if (text == nullptr)
        return -1;

    for(;;)
    {
        if(fscanf (file, "%s", text) == EOF)
        {
            printf("[get_string] Can't read string: EOF");
            return -1;
        }
        else if(strncmp (text, "/*", 2) == 0)
        {
            for(;;)
            {
                l = strlen (text);
                if (l >= 3)
                    l = l - 2;
                else
                    l = 0;
                if (strcmp((text + l), "*/") == 0)
                    break;
                if (fscanf(file, "%s", text) == EOF)
                {
                    printf("[get_string] Can't find end of comment!");
                    return -1;
                }
            }
        } else
            return 0;
    }
}


int get_keyword (FILE * file, const char *keyword)
{
    char text[MAXSTRINGLENGTH];

    /* printf("[get_keyword] Looking for keyword %s",keyword); */

    for (;;)
    {
        if (get_string (file, text) < 0)
        {
            printf ("[get_keyword] Can't find keyword %s!", keyword);
            return -1;
        }
        else if (strcmp (text, keyword) == 0)
            return 0;
#ifdef DEBUG
        else
            printf ("[get_keyword] keyword = %s, text = %s", keyword, text);
#endif
    }
}


int get_int (FILE * file, int *value, int min, int max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%d", value) <= 0)
    {
        printf ("[get_int] Can't read integer value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_int] Integer value outside limits: %d<%d<%d",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_float (FILE * file, float *value, float min, float max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%f", value) <= 0)
    {
        printf ("[get_float] Can't read float value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_float] Float value outside limits: %f<%f<%f",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_double (FILE * file, double *value, double min, double max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%lf", value) <= 0)
    {
        printf ("[get_double] Can't read double value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_double] Double value outside limits: %f<%f<%f",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_long (FILE * file, long *value, long min, long max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%ld", value) <= 0)
    {
        printf ("[get_long] Can't read long value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_long] Long integer value outside limits: %ld<%ld<%ld",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_uint16 (FILE * file, uint16 * value, uint16 min, uint16 max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%hu", value) < 0)
    {
        printf ("[get_uint16] Can't read uint16 value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_uint16] uint16 value outside limits: %hu<%hu<%hu",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_uint32 (FILE * file, uint32 * value, uint32 min, uint32 max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%u", value) < 0)
    {
        printf ("[get_uint32] Can't read uint32 value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_uint32] uint32 value outside limits: %u<%u<%u",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_hex16 (FILE * file, uint16 * value, uint16 min, uint16 max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%hx", value) < 0)
    {
        printf ("[get_hex16] Can't read hex16 value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_hex16] Hex16 value outside limits: 0x%hx<0x%hx<0x%hx",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_hex32 (FILE * file, uint32 * value, uint32 min, uint32 max)
{
    char text[MAXSTRINGLENGTH];

    if (get_string (file, text) < 0)
        return -1;

    if (sscanf (text, "%x", value) < 0)
    {
        printf ("[get_hex32] Can't read hex32 value!");
        return -1;
    }
    if ((*value < min) || (*value > max))
    {
        printf ("[get_hex32] Hex32 value outside limits: 0x%x<0x%x<0x%x",
                min, *value, max);
        return -1;
    }
    return 0;
}


int get_binary (FILE * file, uint16 * value, int no_bits)
{
    char word[MAXSTRINGLENGTH];
    int i;
    uint16 x;

    if (no_bits > 16)
    {
        printf ("[get_binary] Maximum 16 bit word");
        return -1;
    }

    if (get_string (file, word) < 0)
        return -1;

    if (strlen (word) != (unsigned int) no_bits)
    {
        printf ("[get_binary] Wrong number of bits");
        return -1;
    }

    x = 0;
    for (i = 0; i <= no_bits - 1; i++)
    {
        x <<= 1;
        if (word[i] == '1')
        {
            x |= 1;
        }
        else if ((word[i] != '0') && (word[i] != 'x') && (word[i] != 'X'))
        {
            printf ("[get_binary] Undefined character in bitpattern.");
            return -1;
        }
    }
    *value = x;

    return 0;
}


/***********************************************
 * tests if b is equal to a within a tolerance
 ***********************************************/
int fequal (float a, float b)
{
    const float tolerance = 0.001;	// 1 per mille tolerance

    if (fabs (a - b) / a <= tolerance)
        return 1;
    else
        return 0;
}


/***********************************************
 *
 ***********************************************/
FILE * fileopen (const char *fname, const char *type)
{
    FILE *file;
    char defname[256];

    strcpy (defname, BASEDIR);
    strcat (defname, "/Parameter/");
    strcat (defname, fname);

    /* open parameter file */
    file = fopen (defname, type);

    return file;
}


/****************************************************************
 * Checks for keyboard input
 ****************************************************************/
int kbhit (char *c, int sleeptime)
{
    struct timeval tv;
    fd_set in_fd;
    int status;

    tv.tv_sec = sleeptime;
    tv.tv_usec = (sleeptime - (int) sleeptime) * 1000000;

    /* Must be done first to initialize in_fd */
    FD_ZERO (&in_fd);
    /* Makes select() ask if input is ready: 0 is the file descriptor for
     stdin */
    FD_SET (0, &in_fd);

    /* The first parameter is the number of the largest file descriptor to
     check + 1. */
    if (select (1, &in_fd, nullptr, nullptr, &tv) > 0 && read (0, c, 1) == 1)

        status = 1;
    else
        status = -1;

    return status;
}


int kbhit_rawtt (char *c, int sleeptime)
{
    struct timeval tv;
    fd_set in;
    struct termios Savetty;
    struct termios newtty;
    int status;

    if (ioctl (0, TCGETS, &Savetty) == -1)
    {
        printf ("Could not get current tty.\n");
        return -1;
    }

    newtty = Savetty;
    newtty.c_lflag &= ~ICANON;
    newtty.c_lflag &= ~ECHO;
    newtty.c_cc[VMIN] = 1;
    newtty.c_cc[VTIME] = 0;

    if (ioctl (0, TCSETSF, &newtty) == -1)
    {
        printf ("Cannot put tty into raw mode.\n");
        return -1;
    }

    tv.tv_sec = sleeptime;
    tv.tv_usec = (sleeptime - (int) sleeptime) * 1000000;

    FD_ZERO (&in);
    FD_SET (0, &in);
    if (select (1, &in, 0, 0, &tv) > 0 && read (0, c, 1) == 1)
    {
        status = 1;
    }
    else
        status = -1;

    if (ioctl (0, TCSETSF, &Savetty) == -1)
    {
        printf ("Could not set back tty.\n");
        return -1;
    }

    return status;
}


/****************************************************************
 * prints out the corresponding bitpattern of a 16 bit integer
 ****************************************************************/
void bitpattern (unsigned value)
{
    int c, displayMask = 1 << 15;

    for (c = 1; c <= 16; c++)
    {
        putchar (value & displayMask ? '1' : '0');
        value <<= 1;
        if (c == 8)
            putchar (' ');
    }
}


/****************************************************************
 * getbits gives the value of n bits of x starting at position p
 ****************************************************************/
unsigned getbits (unsigned x, int p, int n)
{
    return (x >> p) & ~(~0 << n);
}


/****************************************************************
 * Some routines for terminal output
 ****************************************************************/
#define ESC   0x1b
#define CLR1  0x32
#define LICO  0x59
#define KAKKO 0x5b
#define HOMEP 0x48
#define CLR2  0x4a
#define SEMICO 0x3b
#define HIGH1  0x31
#define HIGH2  0x6d
#define NORM   0x30

void hi_txt (void)
{
    putchar (ESC);
    putchar (KAKKO);
    putchar (HIGH1);
    putchar (HIGH2);

    return;
}

void lo_txt (void)
{
    putchar (ESC);
    putchar (KAKKO);
    putchar (NORM);
    putchar (HIGH2);

    return;
}

void cl_term (void)
{
    putchar (ESC);
    putchar (KAKKO);
    putchar (HOMEP);
    putchar (ESC);
    putchar (KAKKO);
    putchar (CLR1);
    putchar (CLR2);

    return;
}

void home_term (void)
{
    putchar (ESC);
    putchar (KAKKO);
    putchar (HOMEP);

    return;
}

void put_str (int m, int n, char s[80], int i)
{
    int k;

    putchar (ESC);
    putchar (KAKKO);
    printf ("%d", m);
    putchar (SEMICO);
    printf ("%d", n);
    putchar (HOMEP);
    for (k = 0; k < i; k++)
        putchar (s[k]);

    return;
}
