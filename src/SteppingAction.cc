/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/SteppingAction.hh"
#include "Constants.hh"
#include "lsr/PMTHisto.hh"

#include "G4RunManager.hh"
#include "G4SteppingManager.hh"
#include "G4SDManager.hh"
#include "G4EventManager.hh"
#include "G4ProcessManager.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4StepPoint.hh"
#include "G4TrackStatus.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4OpBoundaryProcess.hh"

namespace lsr {

SteppingAction::SteppingAction(HistoManager* hstMngr) {
    histoManager = hstMngr;
}

SteppingAction::~SteppingAction() {}


void SteppingAction::UserSteppingAction(const G4Step * theStep) {
    G4Track* theTrack  = theStep->GetTrack();

    // check if this really is an optical photon
    if (theTrack->GetDefinition() != G4OpticalPhoton::OpticalPhotonDefinition())
        return;

    // G4StepPoint*       thePrePoint  = theStep->GetPreStepPoint();
    // G4VPhysicalVolume* thePrePV     = thePrePoint->GetPhysicalVolume();

    G4StepPoint*       thePostPoint = theStep->GetPostStepPoint();
    G4VPhysicalVolume* thePostPV    = thePostPoint->GetPhysicalVolume();

    if (!thePostPV) return;  // out of world

    G4OpBoundaryProcessStatus   boundaryStatus = Undefined;
    static G4OpBoundaryProcess* boundary       = nullptr;

    // find the boundary process only once
    if (!boundary) {
        G4ProcessManager* pm
                = theStep->GetTrack()->GetDefinition()->GetProcessManager();
        G4int nprocesses = pm->GetProcessListLength();
        G4ProcessVector* pv = pm->GetProcessList();
        G4int i;
        for (i = 0; i < nprocesses; i++) {
            if ((*pv)[i]->GetProcessName() == "OpBoundary") {
                boundary = static_cast<G4OpBoundaryProcess*>((*pv)[i]);
                break;
            }
        }
    }

    boundaryStatus = boundary->GetStatus();

    // Check to see if the partcile was actually at a boundary
    // Otherwise the boundary status may not be valid
    if (thePostPoint->GetStepStatus() == fGeomBoundary && boundaryStatus == Detection) {
        G4String PVName = thePostPV->GetName();
        PMTHisto* histo = nullptr;


        // should be changed that this is depends only on the number of PMT not the name
        if (PVName == "scath0") {
            histo = histoManager->getPMTHisto(0);
        } else if (PVName == "scath1") {
            histo = histoManager->getPMTHisto(1);
        } else if (PVName == "scath2") {
            histo = histoManager->getPMTHisto(2);
        } else {
            return;  // Nothing essential happend, so return
        }
        if (!histo) exit(-3);

        histoManager->N_det++;
        G4ThreeVector coords = theTrack->GetPosition();
        histoManager->halldetpos->Fill(histoManager->r0.x()/mm,
                                       histoManager->r0.y()/mm,
                                       histoManager->r0.z()/mm);
        histo->hdetpos->Fill(histoManager->r0.x()/mm,
                             histoManager->r0.y()/mm,
                             histoManager->r0.z()/mm);

        if (histoManager->isBackgroundParticle) {
            histoManager->N_background_det++;
            histoManager->hbackgroundthetadet->Fill(histoManager->theta*180./M_PI, 1);
            histoManager->hbackgroundphidet->Fill(histoManager->azimuth*180/M_PI, 1);
            histoManager->hbackgroundlambdadet->Fill(histoManager->lambda);
            histoManager->hbackgroundzdet->Fill((histoManager->r0.getZ())/m, 1);

            histo->hbackgroundsegthdet->Fill(histoManager->theta*180./M_PI, 1);  // coordinates should be changed to be relative to current PMT
            histo->hbackgroundsegz0det->Fill((histoManager->r0.getZ())/m, 1);
            histo->hbackgroundsegstartxyd->Fill(histoManager->r0.getX()/mm, histoManager->r0.getY()/mm, 1);
            histo->hbackgroundsegstartxzd->Fill(histoManager->r0.getX()/mm, histoManager->r0.getZ()/mm, 1);

            histo->hbackgroundsegxzd->Fill(coords.getX(), coords.getZ(), 1);
        } else {
            histoManager->N_beam_det++;
            // beam photon from chamber
            if (abs(histoManager->r0.getZ()) <= 0.25*m) {  // should be chamber length dependable
                histoManager->N_beam_chamber_det++;
            }
            histoManager->hbeamthetadet->Fill(histoManager->theta*180./M_PI, 1);
            histoManager->hbeamphidet->Fill(histoManager->azimuth*180/M_PI, 1);
            histoManager->hbeamlambdadet->Fill(histoManager->lambda);
            histoManager->hbeamzdet->Fill((histoManager->r0.getZ())/m, 1);

            histo->hbeamlambdadet->Fill(histoManager->lambda);
            histo->hbeamsegthdet->Fill(histoManager->theta*180./M_PI, 1);  // coordinates should be changed to be relative to current PMT
            histo->hbeamsegz0det->Fill((histoManager->r0.getZ())/m, 1);
            histo->hbeamsegstartxyd->Fill(histoManager->r0.getX()/mm, histoManager->r0.getY()/mm, 1);
            histo->hbeamsegstartxzd->Fill(histoManager->r0.getX()/mm, histoManager->r0.getZ()/mm, 1);

            histo->hbeamsegxzd->Fill(coords.getX(), coords.getZ(), 1);
        }
    }
}

}  // namespace lsr

