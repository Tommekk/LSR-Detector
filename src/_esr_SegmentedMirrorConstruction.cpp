/***********************************************************
 *
 * Simulation of ESR mirror system 
 *
 * Author: V.M. Hannen, based on geant4 N01 example
 * modification date: 18.2.09
 *
 ***********************************************************/

/*********************************************************
 * Construct segmented mirror section
 *********************************************************/
void esr_DetectorConstruction::esr_BuildMirrorSection()
{
  geometry_par_t G = *gpar;

  G4RotationMatrix* RotX90 = new G4RotationMatrix();
  RotX90->rotateX(G4double(-90.*deg));

  //------------------------
  // mirror segments
  //------------------------
  double seg_rad   = 125.0; // beam pipe radius
  double seg_width =  56.0; // width of mirror segment
  
  // construct upper conical mirror part
  double seg_clr   =  97.0; // lower  radius of upper mirror part
  double seg_chr   = 114.0; // higher radius of upper mirror part
  double seg_chgt  =  55.0; // height of upper mirror part

  G4Cons* seg_con = new G4Cons("seg_con",
                               seg_chr,       // inside radius at -pDz
                               seg_chr+2.0,   // outside radius at -pDz
                               seg_clr,       // inside radius at +pDz
                               seg_clr+2.0,   // outside radius at +pDz
                               seg_width/2.0, // pDz
                               0.0*deg,       // pSPhi
                               180.0*deg      // pDPhi
                               );

  //cut off the upper part
  G4Box* seg_box = new G4Box("seg_box", seg_rad, seg_rad/2, seg_width/2+1);

  G4SubtractionSolid* seg_upr =
    new G4SubtractionSolid("seg_upr", seg_con, seg_box, NULL,
                           G4ThreeVector(0, seg_rad/2 + seg_chgt, 0));

  // construct lower "elliptical"/conical mirror part
  double seg_elr   = 102.0; // lower  radius of lower mirror part
  double seg_ehr   = 118.0; // higher radius of lower mirror part
  double seg_ehgt  =  31.0; // height of lower mirror part
  
  G4Cons* seg_elp = new G4Cons("seg_elp",
                               seg_ehr,       // inside radius at -pDz
                               seg_ehr+2.0,   // outside radius at -pDz
                               seg_elr,       // inside radius at +pDz
                               seg_elr+2.0,   // outside radius at +pDz
                               seg_width/2.0, // pDz
                               180.0*deg,     // pSPhi
                               180.0*deg      // pDPhi
                               );
  
  G4SubtractionSolid* seg_lwr =
    new G4SubtractionSolid("seg_lwr", seg_elp, seg_box, NULL,
                           G4ThreeVector(0, seg_rad/2 - seg_ehgt, 0));
  
  // assemble mirror parts
  G4UnionSolid* segment =
    new G4UnionSolid("segment", seg_upr, seg_lwr, NULL, 
                     G4ThreeVector(0., seg_ehgt, 0.));
  
  G4LogicalVolume* segment_log =
    new G4LogicalVolume(segment, Steel, "segment_log");

  // add mirror surface to volume
  new G4LogicalSkinSurface("segment_surface", segment_log, MIRO2_surf);

  double zpos = G.seg_zorig-seg_width/2;
  char   segname[255];
  int    i;
  for (i=0; i<10; i++)
    {
      zpos += seg_width;
      if (i==4 || i==6) zpos += 31;
      sprintf(segname, "segment_%02d", i+1);

      segment_phys[i] = new G4PVPlacement(NULL,
                                          G4ThreeVector(0., 0., zpos),
                                          segment_log, segname,
                                          experimentalHall_log, false, i);
    }

  //----------------------
  // mirror segment lid
  //----------------------
  G.lid_length      = 622.0;  // mirror lid - length
  G.lid_dist        = 199.0;  // mirror distances
  gpar->lid_length  = G.lid_length;
  gpar->lid_dist    = G.lid_dist;

  double lid_width  = 204.0;  // mirror lid - width 
  double lid_thick  =   4.0;  // mirror lid - thickness
  double lid_rad    =  47.5;  // radii of openings in lid

  zpos = G.seg_zorig + G.lid_length/2;

  G4Box* lid_box = 
    new G4Box("lid_box", lid_width/2, lid_thick/2, G.lid_length/2);
  
  G4Tubs* lid_win = new G4Tubs("lid_win", 
                               0.0,           // inner radius
                               lid_rad,       // radius
                               lid_thick,     // half height along z-axis
                               0.*deg,        // start angle
                               360.*deg       // spanning angle
                               );
  
  G4SubtractionSolid* lid_w1 =
    new G4SubtractionSolid("lid_w1", lid_box, lid_win, RotX90,
                           G4ThreeVector(0, 0, -G.lid_dist));
  
  G4SubtractionSolid* lid_w2 =
    new G4SubtractionSolid("lid_w2", lid_w1, lid_win, RotX90, G4ThreeVector(0, 0, 0));
  
  G4SubtractionSolid* lid_w3 =
    new G4SubtractionSolid("lid_w3", lid_w2, lid_win, RotX90,
                           G4ThreeVector(0, 0, G.lid_dist));

  // construct logical volume
  G4LogicalVolume* lid_log = new G4LogicalVolume(lid_w3, Steel, "lid_log");  

  // add mirror surface to lid
  new G4LogicalSkinSurface("miro2_surface", lid_log, MIRO2_surf);

  lid_phys = 
    new G4PVPlacement(NULL, G4ThreeVector(0, seg_chgt+2.0 , zpos),
                      lid_log, "lid", experimentalHall_log, false, 0);
  
  //----------------------
  // viewport windows
  //----------------------
  double vwp_wrad   = 36.0;  // radius of viewport windows
  double vwp_wthk   =  2.0;  // thickness of viewport windows
  double vwp_height = 72.0;  // height of viewport
  double vwp_frad   = 76.0;  // radius of viewport flange
  double vwp_fthk   = 20.0;  // thickness of viewport flange

  G4Tubs* win2 = new G4Tubs("win2", 
                            0.0,        // inner radius
                            vwp_wrad,   // outer radius
                            vwp_wthk/2, // half height along z-axis
                            0.*deg,     // start angle
                            360.*deg    // spanning angle
                            );
  
  G4LogicalVolume* win2_log = new G4LogicalVolume(win2, Glass, "win2_log");
  
  // Define visualization attributes with cyan colour
  G4VisAttributes * win2_VisAtt = new G4VisAttributes(G4Colour(1.,1.,0.));
  win2_log->SetVisAttributes(win2_VisAtt);
  
  // reentrance tubes
  G4Tubs* retub1 = new G4Tubs("retub1", 
                              vwp_wrad,     // inner radius
                              vwp_wrad+1.5, // outer radius
                              vwp_height/2, // half height along z-axis
                              0.*deg,       // start angle
                              360.*deg      // spanning angle
                              );
  
  G4Tubs* retub2 = new G4Tubs("retub2", 
                              vwp_wrad+2.0, // inner radius
                              vwp_frad,     // outer radius
                              vwp_fthk/2,   // thickness along z-axis
                              0.*deg,       // start angle
                              360.*deg      // spanning angle
                              );
  
  G4UnionSolid* retub =
    new G4UnionSolid("retub", retub1, retub2, NULL,
                     G4ThreeVector(0, 0, -vwp_height/2+vwp_fthk/2));
  
  G4LogicalVolume* retub_log = new G4LogicalVolume(retub, Steel, "retub_log");


  G4VisAttributes * retub_VisAtt = new G4VisAttributes();
  //retub_VisAtt->SetForceWireframe(true);
  retub_VisAtt->SetForceAuxEdgeVisible (true);
  retub_log->SetVisAttributes(retub_VisAtt);

  //-------------
  // lightguides 
  //-------------

  double hight_lg = 65*mm;//G.lg_height/2;
  double rad_det = 23*mm;
  G4Cons* lg1 = new G4Cons("lg1", 
                           0.0,      // inner radius at -pDz
                           23,     // outer radius at -pDz
                           0.0,      // inner radius at +pDz
                           36,     // outer radius at +pDz
                           hight_lg,     // half height along z-axis
                           0.*deg,        // start angle
                           360.*deg       // spanning angle
                           );
  
  G4LogicalVolume* lg1_log = new G4LogicalVolume(lg1, PGlass, "lg1_log");
  
  // Define visualization attributes with cyan colour
  G4VisAttributes * lg1_VisAtt = new G4VisAttributes(G4Colour(0.,1.,1.));
  lg1_log->SetVisAttributes(lg1_VisAtt);
  
  lg1_phys = new G4PVPlacement(RotX90, 
                               G4ThreeVector(0, hight_lg+80+2, zpos-G.lid_dist),
                               lg1_log, "lg1", experimentalHall_log, false, 0);
  lg2_phys = new G4PVPlacement(RotX90, 
                               G4ThreeVector(0, hight_lg+80+2, zpos),
                               lg1_log, "lg2", experimentalHall_log, false, 0);
  lg3_phys = new G4PVPlacement(RotX90, 
                               G4ThreeVector(0, hight_lg+80+2, zpos+G.lid_dist),
                               lg1_log, "lg3", experimentalHall_log, false, 0);

  //--------------------------
  // pmt's for mirror section
  //--------------------------
  G4Tubs* spmt = new G4Tubs("spmt", 
                           0.0*cm,         // inner radius
                           25,     // outer radius
                           80.0/2,   // height along z-axis
                           0.*deg,         // start angle
                           360.*deg        // spanning angle
                           );
  
  G4LogicalVolume* spmt_log = new G4LogicalVolume(spmt, Glass, "spmt_log");
  
  // Define visualization attributes with orange colour
  G4VisAttributes * spmt_VisAtt = new G4VisAttributes(G4Colour(1.,0.65,0.));
  //spmt_VisAtt->SetForceWireframe(true);
  // Assignment of the visualization attributes to the logical volume
  spmt_log->SetVisAttributes(spmt_VisAtt);

  //------------------------------------
  // photocathode's for mirror section
  //------------------------------------
  // R1017
  G4Tubs* scath = new G4Tubs("scath", 0.0, rad_det, 1.0/2, 0., 360.*deg);
  G4LogicalVolume* scath_log  =
    new G4LogicalVolume(scath, Steel, "scath_log");

  //CPM 1993P
/*  G4Tubs* scath = new G4Tubs("scath", 0.0, 7.5, 1.0/2, 0., 360.*deg);
  G4LogicalVolume* scath_log  =
    new G4LogicalVolume(scath, Steel, "scath_log");
*/
  // add photosensitive surface to volume
  new G4LogicalSkinSurface("scath_surf", scath_log, Photocath_surf);


// ************************** filter
  G4Tubs* filter = new G4Tubs("filter", 
                              0.0*cm,        // inner radius
                              25,     // outer radius
                              1*mm, // thickness along z-axis
                              0.*deg,        // start angle
                              360.*deg       // spanning angle
                              );
  
  G4LogicalVolume* filter_log = 
    new G4LogicalVolume(filter, Filter, "filter_log");
  
  // Define visualization attributes with cyan colour
  G4VisAttributes * filter_VisAtt = new G4VisAttributes(G4Colour(1.,0.,0.));
  filter_log->SetVisAttributes(filter_VisAtt);


  
  // place elements
  zpos = G.seg_zorig + G.lid_length/2 - G.lid_dist;

/********************************************************
   G4VPhysicalVolume*  filter_phys1 = new G4PVPlacement(RotX90,
                                        G4ThreeVector(0, 80+2+1+2*hight_lg, zpos),
                                        filter_log, "filter1",
                                        experimentalHall_log, false, 0); 
   G4VPhysicalVolume*  filter_phys2 = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+2+1+2*hight_lg, zpos+ G.lid_dist),
                                        filter_log, "filter2",
                                        experimentalHall_log, false, 0); 
   G4VPhysicalVolume*  filter_phys3 = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+2+1+2*hight_lg, zpos + G.lid_dist + G.lid_dist),
                                        filter_log, "filter3",
                                        experimentalHall_log, false, 0); 
********************************************************/

  //pmt window

  G4Tubs* winpmt = new G4Tubs("winpmt", 
                            0.0,        // inner radius
                            25*mm,   // outer radius
                            0.5*mm, // half height along z-axis
                            0.*deg,     // start angle
                            360.*deg    // spanning angle
                            );
  
  G4LogicalVolume* winpmt_log = new G4LogicalVolume(winpmt, Glass, "winpmt_log");
  
  // Define visualization attributes with cyan colour
  G4VisAttributes * winpmt_VisAtt = new G4VisAttributes(G4Colour(1.,1.,0.));
  winpmt_log->SetVisAttributes(winpmt_VisAtt);

  /*********************************************************
   G4VPhysicalVolume*  winpmt_phys1 = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+2+2*hight_lg+2+0.5, zpos),
                                        winpmt_log, "winpmt1",
                                        experimentalHall_log, false, 0); 
   G4VPhysicalVolume*  winpmt_phys2 = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+2+2*hight_lg+2+0.5, zpos+ G.lid_dist),
                                        winpmt_log, "winpmt2",
                                        experimentalHall_log, false, 0); 
   G4VPhysicalVolume*  winpmt_phys3 = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+2+2*hight_lg+2+0.5, zpos + G.lid_dist + G.lid_dist),
                                        winpmt_log, "winpmt3",
                                        experimentalHall_log, false, 0); 
************************************************************/


  for (i=0; i<3; i++)
    {
      sprintf(segname, "win_%02d", i+1);
      win_phys[i] = new G4PVPlacement(RotX90, G4ThreeVector(0, 80+1, zpos),
                         win2_log, segname, experimentalHall_log, false, 0);
      
      sprintf(segname, "retub_%02d", i+1);
      retub_phys[i] = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+36-8, zpos),
                                        retub_log, segname, 
                                        experimentalHall_log, false, 0);
     
      sprintf(segname, "spmt_%02d", i+1);
      spmt_phys[i] = new G4PVPlacement(RotX90, 
                                       G4ThreeVector(0, 80+2+40+1+2*hight_lg+2+1, zpos),
                                       spmt_log, segname,
                                       experimentalHall_log, false, 0); 

      sprintf(segname, "scath_%02d", i+1);
      scath_phys[i] = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+2+0.5+2*hight_lg+2+1, zpos),   //normale pos: 0, 80+0.5+100, zpos ** pos testmessung: 0, 80+0.5+136, zpos
                                        scath_log, segname,
                                        experimentalHall_log, false, 0); 

/*      sprintf(segname, "filter_log_%02d", i+1);
      filter_phys[i] = new G4PVPlacement(RotX90, 
                                        G4ThreeVector(0, 80+1+100, zpos),
                                        filter_log, segname,
                                        experimentalHall_log, false, 0); 
*/

      zpos += G.lid_dist;
    }

  // ************************** filter
/*  G4Tubs* filter = new G4Tubs("filter", 
                              0.0*cm,        // inner radius
                              23.*mm,     // outer radius
                              G.win_thick/2, // thickness along z-axis
                              0.*deg,        // start angle
                              360.*deg       // spanning angle
                              );
  
  G4LogicalVolume* filter_log = 
    new G4LogicalVolume(filter, Filter, "filter_log");
  
  // Define visualization attributes with cyan colour
  G4VisAttributes * filter_VisAtt = new G4VisAttributes(G4Colour(1.,0.,0.));
  filter_log->SetVisAttributes(filter_VisAtt);

   G4VPhysicalVolume* filter_phys = new G4PVPlacement(RotX90, G4ThreeVector(0, 80-4+111*mm, zpos-G.lid_dist), filter_log,"filter", experimentalHall_log, false, 0);

  // ************************** lense
      G4Orb* lns_orb = new G4Orb("lns_orb", G.lns_curv);
      G4Box* lns_box = new G4Box("lns_box", G.lns_curv, 
                                 G.lns_curv, G.lns_curv);
      
      G4SubtractionSolid* lense = 
        new G4SubtractionSolid("lense", lns_orb, lns_box, NULL, 
                               G4ThreeVector (0, 0, G.lns_offs));
      
      G4LogicalVolume* lense_log =
        new G4LogicalVolume(lense, Glass, "lense_log");
      
      // Define visualization attributes with cyan colour
      G4VisAttributes * lense_VisAtt = new G4VisAttributes(G4Colour(0.,1.,1.));
      lense_log->SetVisAttributes(lense_VisAtt);
           
  G4RotationMatrix* RotLX90 = new G4RotationMatrix();
  RotLX90->rotateX(G4double(90.*deg));

      lense_phys = new G4PVPlacement(RotLX90, G4ThreeVector(0, 80-4+102*mm, zpos-G.lid_dist), lense_log,
                                     "lense",  experimentalHall_log, false, 0);
*/


  return;
}
