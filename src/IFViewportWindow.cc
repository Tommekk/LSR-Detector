/****************************************************************
 * Copyright (C) 2016 Dominik Thomas <thomas.dominik@gmail.com>
 ****************************************************************/

#include "lsr/components/IFViewportWindow.hh"

#include "Constants.hh"


namespace lsr {
namespace components {

/**
 * @brief IFViewportWindow::IFViewportWindow
 */
IFViewportWindow::IFViewportWindow() {
    //----------------------
    // IFViewportWindow window (at the top)
    //----------------------
    window_radius   = gpar->vwp_uvwrad;           // radius of IFViewportWindow windows
    window_thickness   = gpar->vwp_ifwthk;        // thickness of IFViewportWindow windows
    height = gpar->vwp_ifheight;                  // height of IFViewportWindow
    flange_radius   = gpar->vwp_ifwflangerad;     // radius of IFViewportWindow flange
    flange_thickness   = gpar->vwp_ifwflangethk;  // thickness of IFViewportWindow flange
}

IFViewportWindow::~IFViewportWindow() {}

G4double IFViewportWindow::getWindowRadius() const {
    return window_radius;
}

G4double IFViewportWindow::getWindowThickness() const {
    return window_thickness;
}

G4double IFViewportWindow::getHeight() const {
    return height;
}

G4double IFViewportWindow::getFlangeRadius() const {
    return flange_radius;
}

G4double IFViewportWindow::getFlangeThickness() const {
    return flange_thickness;
}

G4VSolid *IFViewportWindow::getSolid() {
    return nullptr;  // should be implemented
}

}  // namespace components
}  // namespace lsr

