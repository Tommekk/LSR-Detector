#include <stdio.h>

#include "Constants.hh"
#include "myutils.hh"

int getpar(const char * fname) {
    FILE *file;

    /* open parameter file */
    if ((file = fopen(fname, "r")) == nullptr) {
        printf("[getpar]: Can't open parameter file %s\n", fname);
        return -1;
    }

    /* allocate memory for parameter structures */
    if (tpar == nullptr) {
        tpar = static_cast<transition_par_t *>(
                    malloc(sizeof(transition_par_t)));
        if (nullptr == tpar) return -1;
    }

    if (gpar == nullptr) {
        gpar = static_cast<geometry_par_t *>(
                    malloc(sizeof(geometry_par_t)));
        if (nullptr == gpar) return -1;
    }

    /* showcasemodus */
    if (get_keyword(file, "showcasemode") < 0) return -1;
    if (get_int(file, &gpar->showcasemode, 0, 1e5) < 0) return -1;

    /* get transition parameters */
    if (get_keyword(file, "beta") < 0) return -1;
    if (get_double(file, &tpar->beta, 0, 1) < 0) return -1;
    if (get_keyword(file, "tau") < 0) return -1;
    if (get_double(file, &tpar->tau, 0, 1) < 0) return -1;
    if (get_keyword(file, "lambda") < 0) return -1;
    if (get_double(file, &tpar->lambda, -5, 1e4) < 0) return -1;

    if (get_keyword(file, "N_exc") < 0) return -1;
    if (get_double(file, &tpar->N_exc, 0, 1e16) < 0) return -1;



    if (get_keyword(file, "beam_dist") < 0) return -1;
    if (get_int(file, &tpar->beam_dist, 0, 1e5) < 0) return -1;

    if (get_keyword(file, "beam_width") < 0) return -1;
    if (get_double(file, &tpar->beam_width, -1e4, 1e4) < 0) return -1;
    if (get_keyword(file, "beam_x") < 0) return -1;
    if (get_double(file, &tpar->beam_x, -1e4, 1e4) < 0) return -1;
    if (get_keyword(file, "beam_y") < 0) return -1;
    if (get_double(file, &tpar->beam_y, -1e4, 1e4) < 0) return -1;
    if (get_keyword(file, "beam_z1") < 0) return -1;
    if (get_double(file, &tpar->beam_z1, -1e4, 1e4) < 0) return -1;
    if (get_keyword(file, "beam_z2") < 0) return -1;
    if (get_double(file, &tpar->beam_z2, -1e4, 1e4) < 0) return -1;
    if (get_keyword(file, "background_type") < 0) return -1;
    if (get_int(file, &tpar->backgr_type, 0, 50) < 0) return -1;

    /* get geometry parameters */
    if (get_keyword(file, "s_lsr") < 0) return -1;
    if (get_double(file, &gpar->s_lsr, 0, 1e3) < 0) return -1;
    if (get_keyword(file, "s_beam") < 0) return -1;
    if (get_double(file, &gpar->s_beam, 0, 1e2) < 0) return -1;
    if (get_keyword(file, "beam_rad") < 0) return -1;
    if (get_double(file, &gpar->beam_rad, 0, 1e5) < 0) return -1;


    if (get_keyword(file, "detector_width") < 0) return -1;
    if (get_double(file, &gpar->detector_width, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "detector_height") < 0) return -1;
    if (get_double(file, &gpar->detector_height, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "detector_length") < 0) return -1;
    if (get_double(file, &gpar->detector_length, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "beamhole_radius") < 0) return -1;
    if (get_double(file, &gpar->beamhole_radius, 0, 1e5) < 0) return -1;

    if (get_keyword(file, "mirror_section_type") < 0) return -1;
    if (get_int(file, &gpar->mirror_section_type, 0, 50) < 0) return -1;
    if (get_keyword(file, "mirror_section_height") < 0) return -1;
    if (get_double(file, &gpar->mirror_section_height, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "mirror_section_surface") < 0) return -1;
    if (get_int(file, &gpar->mirror_section_surf, 0, 100) < 0) return -1;
    /*
     * if (get_keyword(file, "mirror_section_length") < 0) return -1;
    if (get_double(file, &gpar->mirror_section_length, 0, 1e5) < 0) return -1;
    */

    /*
    if (get_keyword(file, "CPC_acceptanceAngle") < 0) return -1;
    if (get_double(file, &gpar->CPC_acceptanceAngle, 0, 1e5) < 0) return -1; */
    if (get_keyword(file, "CPC_areas") < 0) return -1;
    if (get_int(file, &gpar->CPC_areas, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "tower_surface") < 0) return -1;
    if (get_int(file, &gpar->tower_surf, 0, 100) < 0) return -1;

    /* viewport window */
    /*
    if (get_keyword(file, "vwp_wrad") < 0) return -1;
    if (get_double(file, &gpar->vwp_wrad, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "vwp_wthk") < 0) return -1;
    if (get_double(file, &gpar->vwp_wthk, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "vwp_height") < 0) return -1;
    if (get_double(file, &gpar->vwp_height, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "vwp_frad") < 0) return -1;
    if (get_double(file, &gpar->vwp_frad, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "vwp_fthk") < 0) return -1;
    if (get_double(file, &gpar->vwp_fthk, 0, 1e5) < 0) return -1;
    */


    /* kodial window */
    if (get_keyword(file, "vwp_ifheight") < 0) return -1;
    if (get_double(file, &gpar->vwp_ifheight, 0, 1e5) < 0) return -1;
    if (get_keyword(file, "vwp_ifwdia") < 0) return -1;
    if (get_double(file, &gpar->vwp_ifwrad, 0, 1e5) < 0) return -1;
    gpar->vwp_ifwrad /= 2;  // In geometry.par it is diameter

    if (get_keyword(file, "vwp_ifwthk") < 0) return -1;
    if (get_double(file, &gpar->vwp_ifwthk, 0, 1e5) < 0) return -1;

    if (get_keyword(file, "vwp_ifwflangedia") < 0) return -1;
    if (get_double(file, &gpar->vwp_ifwflangerad, 0, 1e5) < 0) return -1;
    gpar->vwp_ifwflangerad /= 2;

    if (get_keyword(file, "vwp_ifwflangethk") < 0) return -1;
    if (get_double(file, &gpar->vwp_ifwflangethk, 0, 1e5) < 0) return -1;

    if (get_keyword(file, "vwp_iffocustype") < 0) return -1;
    if (get_int(file, &gpar->vwp_iffocustype, 0, 1e5) < 0) return -1;

    /* saphir window */
    if (get_keyword(file, "vwp_uvheight") < 0) return -1;
    if (get_double(file, &gpar->vwp_uvheight, 0, 1e5) < 0) return -1;

    if (get_keyword(file, "vwp_uvwdia") < 0) return -1;
    if (get_double(file, &gpar->vwp_uvwrad, 0, 1e5) < 0) return -1;
    gpar->vwp_uvwrad /= 2;

    if (get_keyword(file, "vwp_uvwthk") < 0) return -1;
    if (get_double(file, &gpar->vwp_uvwthk, 0, 1e5) < 0) return -1;

    if (get_keyword(file, "vwp_uvwflangedia") < 0) return -1;
    if (get_double(file, &gpar->vwp_uvwflangerad, 0, 1e5) < 0) return -1;
    gpar->vwp_uvwflangerad /= 2;


    if (get_keyword(file, "vwp_uvwflangethk") < 0) return -1;
    if (get_double(file, &gpar->vwp_uvwflangethk, 0, 1e5) < 0) return -1;

    if (get_keyword(file, "vwp_uvfocustype") < 0) return -1;
    if (get_int(file, &gpar->vwp_uvfocustype, 0, 1e5) < 0) return -1;


    fclose(file);

    return 0;
}
