# $Id: GNUmakefile,v 1.1 1999/01/07 16:05:40 gunter Exp $
# --------------------------------------------------------------
# GNUmakefile for examples module.  Gabriele Cosmo, 06/04/98.
# --------------------------------------------------------------

name      := lsr_detector
G4TARGET  := $(name)
G4EXLIB   := true


CPPFLAGS  := -Wall -g $(shell root-config --cflags) -Wno-shadow
#CPPFLAGS  +=  /home/hannen/local/clhep/2.2.0.4/lib
#CPPFLAGS  := -Wall -g -pthread -m64 -I/home/d_thom04/geant4_workdir/include -Wno-shadow
ROOTLIBS  := $(shell root-config --nonew --libs)
EXTRALIBS += $(ROOTLIBS)

#G4TMP := /home/d_thom04/geant4_workdir/tmp

#ifndef G4INSTALL
# #G4INSTALL = ../../..
# G4INSTALL = /nustorage/local/geant4.10.1-ubuntu_14.04_64bit/share/Geant4-10.1.0/geant4make
# #G4INSTALL = /nustorage/local/geant4.10.1-ubuntu_14.04_64bit
#endif

#added
ifndef G4SYSTEM
 G4SYSTEM = Linux-g++
endif

.PHONY: all
all: lib bin

include $(G4INSTALL)/config/binmake.gmk
